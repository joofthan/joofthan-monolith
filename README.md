# Joofthan
- [Build/Dev](#build)
- [**Platform**](#platform-overview)
- [Application Structure](#example-application-structure)
- [All Applications](#applications)
- [Configuration](#configuration)

## Build
- Build: `mvn package`
- Test: `mvn test -Pnoskip`
- Test Dependencies: `mvn verify`
- Run: `java -jar joofthan-monolith.jar`
### Dev Mode
- Run: `mvn quarkus:dev`
- Application Path: http://localhost:8080
- Attach Debugger to Port: 5005

### Dev Endpoints
- Developer UI: http://localhost:8080/q/dev/
- OpenAPI UI: http://localhost:8080/q/swagger-ui/
- H2 Console: http://localhost:8080/h2/

## Platform Overview
**Runtime**
- Quarkus ([Guides](https://quarkus.io/guides/))

**Quarkus Features**
- [**Contexts and Dependency Injection**](https://quarkus.io/guides/cdi) ([Full](https://jakarta.ee/specifications/cdi/4.0/jakarta-cdi-spec-4.0.html))
- [**Persistence**](https://quarkus.io/guides/hibernate-orm) ([Full](https://jakarta.ee/specifications/persistence/3.1/jakarta-persistence-spec-3.1.html))
- [**RESTful Web Services**](https://quarkus.io/guides/resteasy-reactive) ([Full](https://jakarta.ee/specifications/restful-ws/3.1/jakarta-restful-ws-spec-3.1.html))
- [Qute Templating](https://quarkus.io/guides/qute#template-parameter-declarations) ([Full](https://quarkus.io/guides/qute-reference))
- [Config](https://quarkus.io/guides/config-reference) ([Full](https://download.eclipse.org/microprofile/microprofile-config-3.0.1/microprofile-config-spec-3.0.1.html))
- [Liquibase](https://quarkus.io/guides/liquibase)([Full](https://docs.liquibase.com/concepts/changelogs/attributes/column.html) / [Types](https://docs.oracle.com/en/java/javase/17/docs/api/java.sql/java/sql/Types.html))

**Additional Frameworks**
- [PicoCSS Components](https://picocss.com/docs/forms.html)
- [HTMX Frontend](https://htmx.org/examples/) ([Full](https://htmx.org/reference/))
- [jte Templating](https://github.com/casid/jte/blob/main/DOCUMENTATION.md#displaying-data)
- [Playwright Testing](https://playwright.dev/java/docs/pom)

## Example Application Structure
### AppName
- http:
  - localhost:8080/**appname**
- Java: 
  - src/main/java/com/joofthan/apps/**appname**
  - src/test/java/com/joofthan/apps/**appname**
- Properties: 
  - quarkus.datasource."**appname**".jdbc.url
- DB: 
  - src/main/resources/db/**appname**
- Resources:
  - src/main/resources/**appname**
- Public Resources:
  - src/main/resources/META-INF/resources/**appname**

## Applications
- Joofthan ROOT
- Mealplanner
- MyHome

# Documentation
## Used APIs
### FoodData Central JSON
Ingredients API
- https://fdc.nal.usda.gov/download-datasets.html
### Spoonacular
Recipes API
- https://spoonacular.com/food-api/docs
### Trakt
cache entries and last updated to syc api
- https://trakt.docs.apiary.io/#reference/sync/get-last-activity


## Configuration
### All configuration values
#### Common
- joofthan.port=8080
- joofthan.sslport=8081
- joofthan.db.default.user=sa
- joofthan.db.default.password=
- joofthan.data.path=./data
- joofthan.config.path=./config
  - joofthan.cert.jwt.public=./config/publicJwtKey.pem
  - joofthan.cert.jwt.private=./config/privateJwtKey.pem
  - joofthan.cert.ssl.public=./config/cert.pem
  - joofthan.cert.ssl.private=./config/key.pem
- joofthan.domains=http://localhost
- joofthan.admin=

#### APIs
- joofthan.spoonacular.url=https://api.spoonacular.com/
- joofthan.spoonacular.key=
- joofthan.foodjson.foundation https://fdc.nal.usda.gov/fdc-datasets/FoodData_Central_foundation_food_json_2022-10-28.zip
- joofthan.foodjson.fndds=https://fdc.nal.usda.gov/fdc-datasets/FoodData_Central_survey_food_json_2022-10-28.zip
- joofthan.foodjson.legacy=https://fdc.nal.usda.gov/fdc-datasets/FoodData_Central_sr_legacy_food_json_2021-10-28.zip


### Required for Dev
- joofthan.spoonacular.key=

### Required for jar startup
- joofthan.config.path=
- joofthan.data.path=
- joofthan.spoonacular.key=

### Required for Prod
- joofthan.db.default.user=
- joofthan.db.default.password=
- joofthan.data.path=
- joofthan.config.path=
- joofthan.domains=
- joofthan.admin=
- joofthan.spoonacular.key=

## Data
- ./data/test/database.mv.db
- ./data/login/database.mv.db
- ./data/mealplanner/database.mv.db
- ./data/myhome/database.mv.db