# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project does not  adhere to [Semantic Versioning](http://semver.org/).

---
# 2024
## [1.0.0-SNAPSHOT] - 2024-03

### Added

### Changed
- Quarkus 3.8.3 LTS

### Fixed

---
## [1.0.0-SNAPSHOT] - 2024-02

### Added

### Changed
- Quarkus 3.8.0.CR1
### Fixed

---
# 2023
## [1.0.0-SNAPSHOT] - 2023-10

### Added

### Changed
- Quarkus 3.5.0
### Fixed
- Ram usage (Multithek removed)
---

## [1.0.0-SNAPSHOT] - 2023-09

### Added

### Changed
- Quarkus 3.2.6.Final (reverted)
### Fixed

---
## [1.0.0-SNAPSHOT] - 2023-06

### Added

### Changed
- Quarkus 3.1.2.Final
- Mealplanner: [MP-19](https://joofthan.atlassian.net/browse/MP-19): MealEntity an App angleichen
### Fixed

---
## [1.0.0-SNAPSHOT] - 2023-05

### Added

### Changed
- Quarkus 3.0.3.Final
- Mealplanner: [JOOF-227](https://joofthan.atlassian.net/browse/JOOF-227): Database recipe add Details
### Fixed

---
## [1.0.0-SNAPSHOT] - 2023-04

### Added
- Mealplanner: [JOOF-227](https://joofthan.atlassian.net/browse/JOOF-227): Global recipe detail 
### Changed
- Quarkus 3.0.0.CR2
### Fixed

---
## [1.0.0-SNAPSHOT] - 2023-03

### Added
- Mealplanner: [JOOF-226](https://joofthan.atlassian.net/browse/JOOF-226): Global recipe search
- Mealplanner: [JOOF-213](https://joofthan.atlassian.net/browse/JOOF-213): Openapi generator for Spoonacular
### Changed
- Quarkus 3.0.0.CR1
- Persistence: [JOOF-224](https://joofthan.atlassian.net/browse/JOOF-224): Liquibase migration (Hibernate 5 -> 6)
### Fixed

---

## [1.0.0-SNAPSHOT] - 2023-02

### Added
- Mealplanner: Recipe API
### Changed
- Quarkus 3 from Alpha2 to Alpha4
- Mealplanner: Save Shoplist data to DB
### Fixed
- Mealplanner: Shoplist Endpoint Status codes

---
## [1.0.0-SNAPSHOT] - 2023-01

### Added
- Mealplanner: Shoplist Endpoint (ram only)
- Save log to DB
### Changed

### Fixed

---
# 2022
## [1.0.0-SNAPSHOT] - 2022-12

### Added

### Changed
- Quarkus 3.0.0.Alpha2
- Login: Rename textfield: "user" to "username"
### Fixed

---
## [1.0.0-SNAPSHOT] - 2022-11

### Added

### Changed
- Multithek: Redirect if not logged in
### Fixed
- Login: Enter not working

---
## [1.0.0-SNAPSHOT] - 2022-10

### Added
- Add Mealplanner: Meal Endpoint
- Swagger UI
### Changed
- Quarkus 2.14
- MyHome: Auto reload status
### Fixed

---
## [1.0.0-SNAPSHOT] - 2022-09

### Added
- Add Mealplanner: Meal Endpoint
- Swagger UI
### Changed
- Quarkus 2.13
### Fixed


---
## [1.0.0-SNAPSHOT] - 2022-08

### Added
- MyHome: Settings for Mealtime and how many Days are plannable in future
- MyHome: Status page
### Changed
- Quarkus 2.12
- Switch from JSF to HTMX
### Fixed

---
## [1.0.0-SNAPSHOT] - 2022-07
### Added
- Multithek: Providers structure
- Multithek: Live Provider Examples
- MyHome: Planner and Notes
- Login: Redirect and Autologin
### Changed
- Quarkus 2.11
- Login: Save TestUsers with hashed Password
### Fixed

---
##  [1.0.0-SNAPSHOT] - 2022-06


### Added
- Multithek: Login and Template
