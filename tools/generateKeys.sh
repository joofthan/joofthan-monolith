# SSL
openssl req -newkey rsa:2048 -new -nodes -x509 -days 3650 -keyout config/key.pem -out config/cert.pem
# JWT
openssl genrsa -out ./target/rsaPrivateKey.pem 2048
openssl rsa -pubout -in ./target/rsaPrivateKey.pem -out config/publicJwtKey.pem
openssl pkcs8 -topk8 -nocrypt -inform pem -in ./target/rsaPrivateKey.pem -outform pem -out config/privateJwtKey.pem