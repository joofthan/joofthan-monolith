package com.joofthan.apps.dev;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ProjectStatus {
    PROTOTYPE("Prototype"), PROD("Prod");
    private final String title;
}
