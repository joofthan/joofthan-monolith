package com.joofthan.apps.dev;

import com.joofthan.lib.template.Template;
import jakarta.inject.Inject;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import static com.joofthan.apps.dev.Project.project;

@Path("/dev")
public class DevResource {

    @Inject
    Template template;

    @GET
    @Produces(MediaType.TEXT_HTML)
    public String home(){
        List<Project> projects = List.of(
                project("Dev","dev", true),
                project("Login","login", true),
                project("Mealplanner","mealplanner", "/q/swagger-ui/#/Meal%20Planner%20Endpoint"),
                project("Myhome","myhome")

        );
        //lang="html"
        return template.render("""
                <!DOCTYPE html>
                <html xmlns="http://www.w3.org/1999/xhtml">
                    <head>
                        <title>Dev</title>
                        <link rel="stylesheet" href="../shared/css/style.css"/>
                        <script src="/webjars/htmx.org/dist/htmx.min.js"></script>
                    </head>
                    <body style="text-align: center; font-size: 2em;">
                        <span hx-get="/login/html/me" hx-trigger="load" id="user"
                              style="font-size: 1rem;text-align: right;width: 100%;display: block"></span>
                        <b>Home(<a href="/" class="link">/</a>)</b><br>
                        <h3>Apps</h3>
                        {#each project in projects}
                            {#if project.status.title == "Prod"}
                                {project.name}(<a href="/{project.slug}" class="link">/{project.slug}</a>{project.customHtml})<br/>
                            {#else}
                                {project.name}(<a href="/{project.slug}" class="link">/{project.slug}</a>{project.customHtml})({project.status.title})<br/>
                            {/if}
                        {/each}
                        <br/>
                        <span style="font-size: 0.5em">Data in prototypes will be reset at any time without warning.</span>
                        <h3>Other</h3>
                        Dev UI(<a href="/q/dev" class="link">/q/dev</a>)<br/>
                        Openapi Ui(<a href="/q/swagger-ui" class="link">/q/swagger-ui</a>)<br/>
                        Database(<a href="/h2" class="link">/h2</a>)<br/>
                        (jdbc:h2:file:./target/database)
                        </br></br></br>
                    <span style="font-size: 1rem" hx-get="/dev/api/version" hx-trigger="load"></span>
                    </body>
                </html>
                """, Map.of(
                        "projects", projects
        ));
    }

    @GET
    @Path("/api/version")
    @Produces(MediaType.TEXT_PLAIN)
    public String hello() {

        String version="Not Available";
        Properties p = new Properties();
        try {
            InputStream resourceAsStream = this.getClass().getClassLoader().getResourceAsStream("META-INF/maven/io.quarkus/quarkus-core/pom.properties");
            if(resourceAsStream != null) {
                p.load(resourceAsStream);
                version = p.getProperty("version");
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return "Quarkus "+version;
    }

}