package com.joofthan.apps.dev;

record Project(
String name,
String slug,
ProjectStatus status,
String customHtml
) {

    public static Project project(String name, String slug){
        return new Project(name,slug, ProjectStatus.PROTOTYPE,"");
    }
    public static Project project(String name, String slug, String documentation){
        return new Project(name,slug, ProjectStatus.PROTOTYPE,", <a href='"+documentation+"' class='link'>Doc</a>");
    }
    public static Project project(String name, String slug, boolean isReady){
        return new Project(name,slug,isReady? ProjectStatus.PROD : ProjectStatus.PROTOTYPE,"");
    }
}
