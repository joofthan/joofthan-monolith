package com.joofthan.apps.myhome.note.boundary;

import com.joofthan.apps.myhome.note.control.NoteService;

import jakarta.inject.Inject;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;

@Path("/myhome/api/note")
public class NoteResource {

    @Inject
    NoteService noteService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public NoteResponse get(){
        var note = noteService.load();
        return new NoteResponse(note.getText());
    }
    public record NoteResponse(String text){}


    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public SaveNoteRequest post(SaveNoteRequest req){
        var note = noteService.load();
        note.setText(req.text());
        noteService.save(note);
        return req;
    }

    public static record SaveNoteRequest(
            String text
    ) {    }
}
