package com.joofthan.apps.myhome.note.control;

import com.joofthan.lib.user.UserService;
import com.joofthan.apps.myhome.note.entity.Note;

import jakarta.enterprise.context.RequestScoped;
import jakarta.inject.Inject;

@RequestScoped
public class NoteService {
    @Inject
    UserService userService;
    @Inject
    NoteRepository noteRepository;

    public Note load(){
        var note = noteRepository.findByUserId(userService.getUser().getUserId());
        if(note == null){
            note = new Note();
            note.setText("");
        }
        return note;
    }

    public Note save(Note note){
        if(note.getUserId() == null){
            note.setUserId(userService.getUser().getUserId().id());
        }
        return noteRepository.merge(note);
    }

}
