package com.joofthan.apps.myhome.note.control;

import com.joofthan.lib.user.UserId;
import com.joofthan.apps.myhome.note.entity.Note;

import jakarta.enterprise.context.RequestScoped;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.transaction.Transactional;

@Transactional
@RequestScoped
public class NoteRepository {
    @Inject
    EntityManager em;

    public Note findByUserId(UserId userid){
        return em.find(Note.class,userid.id());
    }

    public Note merge(Note note){
        return em.merge(note);
    }
}
