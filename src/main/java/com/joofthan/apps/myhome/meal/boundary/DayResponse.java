package com.joofthan.apps.myhome.meal.boundary;

import java.util.List;

public record DayResponse(
        String title,
        List<MealResponse> meals
) {
    public boolean isEmpty() {
        for (MealResponse m:meals){
            if (!m.meal().isEmpty()){
                return false;
            }
        }
        return true;
    }
}
