package com.joofthan.apps.myhome.meal.control;

import com.joofthan.apps.myhome.BaseRepository;
import com.joofthan.apps.myhome.meal.entity.MealTimeSettings;

import jakarta.enterprise.context.RequestScoped;
import jakarta.transaction.Transactional;

@RequestScoped
@Transactional
public class MealTimeRepository extends BaseRepository<MealTimeSettings> {
    protected MealTimeRepository() {
        super(MealTimeSettings.class);
    }
}
