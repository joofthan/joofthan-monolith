package com.joofthan.apps.myhome.meal.control;

import com.joofthan.apps.myhome.BaseRepository;
import com.joofthan.apps.myhome.meal.entity.Meal;
import jakarta.enterprise.context.RequestScoped;
import jakarta.transaction.Transactional;

import java.util.List;

@Transactional
@RequestScoped
public class MealRepository extends BaseRepository<Meal> {


    protected MealRepository() {
        super(Meal.class);
    }

    @Deprecated
    @Override
    public List<Meal> findAllForUser(){
        return super.findAllForUser();
    }

    public List<Meal> findTodayAndFutureMeals(){
        Long userId = userService.getUser().getUserId().id();
        return em.createQuery("select m from Meal m where m.date >= current_date and m.userId = :userId",Meal.class)
                .setParameter("userId",userId)
                .getResultList();
    }

    public void delete(Long mealId) {
        em.remove(findById(mealId));
    }
}
