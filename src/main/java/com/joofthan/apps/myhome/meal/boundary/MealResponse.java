package com.joofthan.apps.myhome.meal.boundary;

import com.joofthan.apps.myhome.meal.entity.Meal;

import java.time.LocalDate;
import java.time.LocalTime;

public record MealResponse(
        Long id,
        LocalDate date,
        LocalTime time,
        String title,
        String content,
        boolean shopped,
        boolean eaten,
        boolean otherPlan,
        Meal meal
) {
    public static MealResponse of(Meal m, String generatedTitle){
        Long id1 = m.getId();
        //if(id1 == null) throw new IllegalArgumentException();
        return new MealResponse(id1,m.getDate(),m.getTime(),generatedTitle,nullToString(m.getContent()),m.isShopped(),m.isEaten(),m.isOtherPlan(), m);
    }


    private static String nullToString(String value){
        if(value == null) return "";
        else return value;
    }

    public Meal toMeal() {
        Meal meal = new Meal(
                date,
                time,
                content,
                shopped,
                eaten,
                otherPlan
        );
        meal.setId(id);
        return meal;
    }
}
