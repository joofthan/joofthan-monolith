package com.joofthan.apps.myhome.meal.entity;

import com.joofthan.apps.myhome.BaseEntity;
import lombok.*;

import jakarta.persistence.Entity;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;


@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
public class Meal extends BaseEntity {
    private LocalDate date;
    private LocalTime time;
    private String content;
    private boolean shopped;
    private boolean eaten;
    private boolean otherPlan;

    public LocalDateTime getDateTime(){
        if(date == null || time== null){
            return null;
        }
        return date.atTime(time);
    }
    public void setDateTime(LocalDateTime localDateTime){
        date = localDateTime.toLocalDate();
        time = localDateTime.toLocalTime();
    }

    public boolean isEmpty() {
        return isEmpty(content)
                && shopped == false
                && eaten == false
                && otherPlan == false;
    }
/*
    public MealResponseResource toResponseResource() {
        return new MealResponseResource(this.carbs, this.protein, this.vegetables, this.shopped, this.eaten, this.otherPlan);
    }

    public static Meal fromMealRequestResource(final MealRequestResource resource) {
        return new Meal(
                1L,
                1000L,
                LocalDateTime.now(),
                resource.getCarbs(),
                resource.getProtein(),
                resource.getVegetables(),
                resource.isShopped(),
                resource.isEaten(),
                resource.isOtherPlan()
        );
    }
*/
    private static boolean isEmpty(String value) {
        if (value == null) return true;
        return value.isEmpty();
    }

    public boolean isAllTextFilled() {
        return !isEmpty(content);
    }

    public String getContent() {
        return content;
    }
}
/*
//TODO Record!
@AllArgsConstructor
@Getter
class MealRequestResource {
    private final String carbs;
    private final String protein;
    private final String vegetables;
    private final boolean shopped;
    private final boolean eaten;
    private final boolean otherPlan;
}

//TODO Record!
@AllArgsConstructor
@Getter
class MealResponseResource {
    private final String carbs;
    private final String protein;
    private final String vegetables;
    private final boolean shopped;
    private final boolean eaten;
    private final boolean otherPlan;
}
*/