package com.joofthan.apps.myhome.meal.boundary;

import com.joofthan.lib.Headers;
import com.joofthan.lib.template.Template;
import com.joofthan.apps.myhome.meal.control.MealRepository;
import com.joofthan.apps.myhome.meal.control.MealService;
import com.joofthan.apps.myhome.meal.control.MealTimeRepository;
import com.joofthan.apps.myhome.meal.entity.HomeEntry;
import com.joofthan.apps.myhome.meal.entity.Meal;
import com.joofthan.apps.myhome.meal.entity.MealTimeSettings;

import jakarta.enterprise.context.RequestScoped;
import jakarta.inject.Inject;
import jakarta.ws.rs.HeaderParam;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.List;

@Path("myhome/api/status")
@RequestScoped
public class StatusEndpoint {

    @Inject
    MealRepository mealRepository;
    @Inject
    MealTimeRepository mealTimeRepository;
    @Inject
    MealService mealService;
    @Inject
    Template template;

    List<Meal> _orderedMeals;
    MealTimeSettings _mealTimeSettings;

    public List<Meal> getOrderedMeals() {
        if(_orderedMeals == null){
            _orderedMeals = mealRepository.findTodayAndFutureMeals().stream().sorted(Comparator.comparing(Meal::getDateTime)).toList();
        }
        return _orderedMeals;
    }

    public MealTimeSettings getMealTimeSettings() {
        if(_mealTimeSettings == null){
            _mealTimeSettings = mealTimeRepository.findForUser().orElse(new MealTimeSettings());
        }
        return _mealTimeSettings;
    }


    private HomeEntry getPlanningStatus() {
        boolean isNearestMeal = true;
        for (int i = 0; i < Math.min(getOrderedMeals().size(), getMealTimeSettings().getFutureDays() * getMealTimeSettings().getTimes().size()); i++) {
            Meal m = getOrderedMeals().get(i);
            if(m.getDateTime().isBefore(LocalDateTime.now()))continue;
            if(m.isOtherPlan()){
                isNearestMeal =false;
                continue;
            }
            if(!m.isAllTextFilled()){
                if(isNearestMeal){
                    return HomeEntry.error("Planning");
                }else{
                    return HomeEntry.warning("Planning");
                }
            }
            isNearestMeal = false;
        }

        return HomeEntry.checked("Planning");
    }

    @POST
    @Path("next")
    @Produces({MediaType.TEXT_HTML})
    public String next() {


        if(!getEatenStatus().isOk()){
            return "<div class=\"entry\"><i class=\"fa fa-arrow-right\" style=\"color: orange\"></i><span>Eating</span></div>";
        }
        if(!getPlanningStatus().isOk()){
            return "<div class=\"entry\"><i class=\"fa fa-arrow-right\" style=\"color: orange\"></i><span>Planning</span></div>";
        }
        if(!getShoppedStatus().isOk()){
            return "<div class=\"entry\"><i class=\"fa fa-arrow-right\" style=\"color: orange\"></i><span>Shopping</span></div>";
        }

        return "<div class=\"entry\"><i class=\"fa fa-arrow-right\" style=\"color: green\"></i><span>Enjoy :)</span></div>";
    }

    @POST
    @Path("system")
    @Produces({MediaType.APPLICATION_JSON,MediaType.TEXT_HTML})
    public Response system(@HeaderParam(Headers.HX_REQUEST) boolean hx) {
        if(hx){
            return Response.ok(
                    template.render(PlanStatusEndpoint.ENTRIES_TEMPLATE,
                    "entries",getSystemStatus()),
                    MediaType.TEXT_HTML
            ).build();
        }
        return Response.ok(getSystemStatus(), MediaType.APPLICATION_JSON).build();
    }

    private List<HomeEntry> getSystemStatus() {
        String mainTitle = "Status";
        HomeEntry plan = getPlanningStatus();
        HomeEntry shop = getShoppedStatus();
        HomeEntry eaten = getEatenStatus();
        if(plan.isError() || shop.isError() || eaten.isError()) {
            return List.of(HomeEntry.error(mainTitle + ": Critical"));
        }
        if(plan.isWarning() || shop.isWarning() || eaten.isWarning()) {
            return List.of(HomeEntry.warning(mainTitle + ": Unstable"));
        }
        return List.of(HomeEntry.checked(mainTitle + ": Healthy"));
    }

    @POST
    @Path("meal")
    @Produces({MediaType.APPLICATION_JSON,MediaType.TEXT_HTML})
    public Response getHomeList(@HeaderParam(Headers.HX_REQUEST) boolean hx) {
        var ent = List.of(
                getPlanningStatus(),
                getShoppedStatus(),
                getEatenStatus()
        );
        if(hx){
            return Response.ok(
                    template.render(PlanStatusEndpoint.ENTRIES_TEMPLATE,
                            "entries",ent),
                    MediaType.TEXT_HTML
            ).build();
        }
        return Response.ok(ent,MediaType.APPLICATION_JSON).build();
    }

    private HomeEntry getEatenStatus() {
        Meal mealBefore = null;
        Meal lastMeal=null;
        for (int i = 0; i < Math.min(getOrderedMeals().size(), getMealTimeSettings().getFutureDays() * getMealTimeSettings().getTimes().size()); i++) {
            Meal m = getOrderedMeals().get(i);
            if(m.getDateTime().isAfter(LocalDateTime.now())) break;
            if(lastMeal != null)mealBefore = lastMeal;
            lastMeal = m;
        }
        if(lastMeal==null)return HomeEntry.checked("Eaten");
        if(lastMeal.isEaten())return HomeEntry.checked("Eaten");

        if(mealBefore!=null &&!mealBefore.isEaten())return HomeEntry.error("Eaten");
        return HomeEntry.warning("Eaten");
    }

    private HomeEntry getShoppedStatus() {
        for (int i = 0; i < Math.min(getOrderedMeals().size(), getMealTimeSettings().getFutureDays() * getMealTimeSettings().getTimes().size()); i++) {
            Meal m = getOrderedMeals().get(i);
            if(m.getDateTime().isBefore(LocalDateTime.now())) continue;
            if(m.isOtherPlan())continue;
            if(!m.isShopped()){
                    return HomeEntry.warning("Shopped");
            }
        }

        return HomeEntry.checked("Shopped");
    }
}
