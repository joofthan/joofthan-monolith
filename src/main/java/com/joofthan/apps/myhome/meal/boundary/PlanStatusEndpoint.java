package com.joofthan.apps.myhome.meal.boundary;

import com.joofthan.lib.Headers;
import com.joofthan.lib.template.Template;
import com.joofthan.apps.myhome.meal.entity.HomeEntry;

import jakarta.enterprise.context.RequestScoped;
import jakarta.inject.Inject;
import jakarta.ws.rs.HeaderParam;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import java.util.List;

@Path("myhome/api/status/plan")
@RequestScoped
public class PlanStatusEndpoint {

    static final String ENTRIES_TEMPLATE = """
            {#each entries}
                <div class="entry">
                    <i class="{it.icon}" style="color: {it.color}"></i>
                    <span>{it.text}</span>
                </div>
            {/each}
            """;
    static final String ENTRIES_TEMPLATE_m = """
                <div foreach="entries as e" class="entry">
                    <i class="{e.icon}" style="color: {e.color}"></i>
                    <span>{e.text}</span>
                </div>
            """;
    @Inject
    Template template;

    List<HomeEntry> otherList = List.of(
            HomeEntry.empty("One Thing"),
            HomeEntry.disabled("Review")//darkorange
    );


    @POST
    @Produces({MediaType.APPLICATION_JSON,MediaType.TEXT_HTML})
    public Response otherList(@HeaderParam(Headers.HX_REQUEST) boolean hx) {
        if(hx){
            return Response.ok(
                    template.render(ENTRIES_TEMPLATE,"entries",otherList),
                    MediaType.TEXT_HTML
            ).build();
        }

        return Response.ok(otherList,MediaType.APPLICATION_JSON).build();
    }
}
