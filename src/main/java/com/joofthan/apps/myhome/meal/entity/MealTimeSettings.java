package com.joofthan.apps.myhome.meal.entity;

import com.joofthan.lib.user.UserId;
import com.joofthan.apps.myhome.BaseEntity;
import lombok.Getter;
import lombok.Setter;

import jakarta.persistence.ElementCollection;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
public class MealTimeSettings extends BaseEntity {
    @ElementCollection(fetch = FetchType.EAGER)
    private List<LocalTime> times;
    private int futureDays = 2;
    private int mealsADay = 3;

    public MealTimeSettings(){
        this(null);
    }

    public MealTimeSettings(UserId userId){
        this(userId, new ArrayList<>(
                List.of(
                LocalTime.of(6,0),
                LocalTime.of(12,0),
                LocalTime.of(18,0))
                )
        );
    }

    public MealTimeSettings(UserId userId, List<LocalTime> time){
        if(userId != null)this.userId = userId.id();
        this.times = time;
    }

    public List<LocalTime> getTimes() {
        int previousMealsADay = times.size();
        if(previousMealsADay > mealsADay){
            times = times.subList(0,mealsADay);
        }else if(previousMealsADay<mealsADay){
            while (times.size() < mealsADay){
                times.add(times.get(times.size()-1));
            }
        }
        return times;
    }

    public void setMealsADay(int mealsADay) {
        if(mealsADay<1){
            throw new IllegalArgumentException();
        }
        this.mealsADay = mealsADay;
    }
    public int getMealsADay() {
        if(mealsADay < 1){
            mealsADay = 1;
        }
        return mealsADay;
    }
}
