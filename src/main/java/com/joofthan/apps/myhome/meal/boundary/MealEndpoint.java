package com.joofthan.apps.myhome.meal.boundary;

import com.joofthan.apps.myhome.meal.control.MealService;
import com.joofthan.apps.myhome.meal.entity.Meal;
import lombok.RequiredArgsConstructor;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import java.util.List;

@RequiredArgsConstructor
@Path("myhome/api/meal")
public class MealEndpoint {

    final MealService mealService;

    @GET
    public List<DayResponse> get(){
        return mealService.getToday();
    }
    @POST
    public Meal save(Meal m){
        return mealService.save(m);
    }

    @GET
    @Path("{id}")
    public Meal load(@PathParam("id") Long mealId){
        return mealService.load(mealId);
    }


}
