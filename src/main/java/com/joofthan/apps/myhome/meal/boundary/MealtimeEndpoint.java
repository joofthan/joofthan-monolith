package com.joofthan.apps.myhome.meal.boundary;

import com.joofthan.apps.myhome.meal.control.MealService;
import com.joofthan.apps.myhome.meal.entity.MealTimeSettings;

import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

@Transactional
@Path("/myhome/api/mealtime")
public class MealtimeEndpoint {
    @Inject
    MealService mealService;

    @GET
    @Produces({MediaType.APPLICATION_JSON,MediaType.TEXT_HTML})
    public Response getMealTimes() {
        MealTimeSettings mealTimeSettings = mealService.getMealTimeSettings();
        return Response.ok(mealTimeSettings, MediaType.APPLICATION_JSON).build();
    }


    @POST
    public MealTimeSettings save(MealTimeSettings mealTimeSettings) {
        return mealService.save(mealTimeSettings);
    }
}
