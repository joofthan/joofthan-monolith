package com.joofthan.apps.myhome.meal.control;

import com.joofthan.lib.user.UserId;
import com.joofthan.lib.user.UserService;
import com.joofthan.apps.myhome.meal.boundary.DayResponse;
import com.joofthan.apps.myhome.meal.boundary.MealResponse;
import com.joofthan.apps.myhome.meal.entity.Meal;
import com.joofthan.apps.myhome.meal.entity.MealTimeSettings;
import lombok.RequiredArgsConstructor;

import jakarta.enterprise.context.RequestScoped;
import jakarta.transaction.Transactional;
import java.time.LocalDate;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@Transactional
@RequestScoped
public class MealService {
    final UserService userService;
    final MealTimeRepository mealTimeRepository;
    final MealRepository mealRepository;

    public List<DayResponse> getToday(){
        List<Meal> meals = mealRepository.findTodayAndFutureMeals();
        var mealTimeSettings = getMealTimeSettings();

        if(meals.isEmpty() || ! meals.get(meals.size() - 1).isEmpty()){
            meals.addAll(createRequiredMealsForFutureSettings(meals, mealTimeSettings).stream().map(mealRepository::merge).toList());
        }

        Meal lastMeal = meals.get(meals.size() - 1);
        if (lastMeal != null && !lastMeal.isEmpty()){
            meals.addAll(createNextDayMeals(mealTimeSettings,lastMeal.getDate()).stream().map(mealRepository::merge).toList());
        }

        meals = meals.stream().sorted(Comparator.comparing(Meal::getDate)).toList();

        List<MealResponse> result = new ArrayList<>();
        int i = 0;
        String[] names = {"Breakfast","Lunch","Dinner"};
        for (Meal m:meals){
            String title = "Meal "+((i % mealTimeSettings.getMealsADay())+1);
            if(mealTimeSettings.getMealsADay() == 1) title = "";
            if(mealTimeSettings.getMealsADay() % 3 == 0) title = names[i% 3];
            result.add(MealResponse.of(m,title));
            i++;
        }

        List<DayResponse> days = new ArrayList<>();
        final AtomicInteger counter = new AtomicInteger(0);
        var res =  result.stream()
                .collect(Collectors.groupingBy(s -> counter.getAndIncrement()/ mealTimeSettings.getMealsADay()))
                .values().stream().toList();
        var today = LocalDate.now().getDayOfWeek();
        for (int j = 0; j < res.size(); j++) {
            List<MealResponse> m = res.get(j);
            String day = today.plus(j).name();
            days.add(new DayResponse(day.toUpperCase().charAt(0)+day.substring(1).toLowerCase(), m));
        }
        if(days.size()>=2){
            DayResponse lastDay = days.get(days.size() - 1);
            DayResponse preLastDay = days.get(days.size() - 2);
            if(lastDay.isEmpty() && preLastDay.isEmpty()){
                lastDay.meals().forEach(m->mealRepository.delete(m.id()));
                days.remove(lastDay);
            }
        }

        return days;
    }

    private List<Meal> createRequiredMealsForFutureSettings(List<Meal> meals, MealTimeSettings mealTimeSettings) {
        Meal lastMeal = meals.isEmpty()? null: meals.get(meals.size() - 1);
        long futureDays = mealTimeSettings.getFutureDays();
        for (int i = 0; i < futureDays+1; i++) {
            LocalDate dateToCreate = LocalDate.now().plusDays(i);
            if(lastMeal != null) {
                boolean isInAlreadyGeneratedTimespan = dateToCreate.isBefore(lastMeal.getDate()) || dateToCreate.isEqual(lastMeal.getDate());
                if (isInAlreadyGeneratedTimespan) continue;
            }
            return createNextDayMeals(mealTimeSettings, dateToCreate);
        }
        return List.of(); //No new meals required
    }

    private List<Meal> createNextDayMeals(MealTimeSettings mealTimeSettings, LocalDate dateToCreate) {
        List<Meal> mealsOfDay = new ArrayList<>();
        for(var currentMealTime : mealTimeSettings.getTimes()){
            Meal meal = new Meal();
            meal.setUserId(userService.getUser().getUserId().id());
            meal.setDateTime(dateToCreate.atTime(currentMealTime));
            mealsOfDay.add(meal);
        }
        return mealsOfDay;
    }

    public MealTimeSettings getMealTimeSettings(){
        UserId userId = userService.getUser().getUserId();
        MealTimeSettings byUser = mealTimeRepository.findForUser().orElse(null);
        if(byUser==null){
            byUser=mealTimeRepository.merge(new MealTimeSettings(userId));
        }
        return byUser;
    }

    public MealTimeSettings save(MealTimeSettings mealTimeSettings) {
        return mealTimeRepository.merge(mealTimeSettings);
    }

    public Meal save(Meal meal) {
        meal.setUserId(userService.getUser().getUserId().id());
        return mealRepository.merge(meal);
    }

    public Meal load(Long mealId) {
        return mealRepository.findById(mealId);
    }
}
