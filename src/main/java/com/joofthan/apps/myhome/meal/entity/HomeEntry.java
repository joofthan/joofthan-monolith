package com.joofthan.apps.myhome.meal.entity;

public record HomeEntry(
        String text,
        String icon,
        String color
) {

    public static HomeEntry checked(String title){
        return new HomeEntry(title, "fa fa-circle-check", "darkgreen");
    }

    public static HomeEntry warning(String title){
        return new HomeEntry(title, "fa fa-circle-exclamation", "darkorange");
    }

    public static HomeEntry error(String title){
        return new HomeEntry(title, "fa fa-circle-xmark", "red");
    }

    public static HomeEntry empty(String title){
        return new HomeEntry(title, "fa fa-circle-question", "darkgreen");
    }

    public static HomeEntry disabled(String title){
        return new HomeEntry(title, "fa fa-circle-exclamation", "gray");//darkorange
    }

    public boolean isOk() {
        return "darkgreen".equals(color);
    }
    public boolean isWarning() {
        return "darkorange".equals(color);
    }
    public boolean isError() {
        return "red".equals(color);
    }
}
