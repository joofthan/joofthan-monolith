package com.joofthan.apps.myhome.html;

import com.joofthan.lib.template.Template;
import com.joofthan.apps.myhome.meal.boundary.DayResponse;
import com.joofthan.apps.myhome.meal.boundary.MealEndpoint;
import com.joofthan.apps.myhome.meal.entity.Meal;

import jakarta.inject.Inject;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import java.util.List;

@Path("/myhome/html/dayplanner")
@Produces(MediaType.TEXT_HTML)
public class Dayplanner {
    @Inject
    Template template;

    @Inject
    MealEndpoint mealEndpoint;

    @GET
    public String get() {
        List<DayResponse> days = mealEndpoint.get();

        return template.render("""
            <div>
            {#for day in days}
                <h1>{day.title}</h1>
                {#for meal in day.meals}
                    <h3>{meal.title}</h3>
                    <form hx-post="/myhome/html/dayplanner/save" hx-trigger='keyup, change' hx-swap='none'>
                        <input name='id' type='hidden' value='{meal.id}'>
                        <input name="content"  placeholder="What is planned?" type="text" value="{meal.content}" style="margin-bottom: 1em; width: calc(100% - 2em);" class="autostyle">
                        <input name="shopped" id="shopped" type="checkbox" {meal.shopped ? 'checked' : ''} style="margin-bottom: 1em;"><label for="shopped">Shopped</label>
                        <input name="eaten" id="eaten" type="checkbox" {meal.eaten ? 'checked' : ''} style="margin-bottom: 1em;"><label for="eaten">Eaten</label>
                        <input name="otherPlan" id="otherPlan" type="checkbox" {meal.otherPlan ? 'checked' : ''} style="margin-bottom: 1em;"><label for="otherPlan">Other plan</label>
                    </form>
                {/for}
            {/for}
            ""","days",days);
    }

    @POST
    @Path("save")
    public void save(@FormParam("id") Long id,
                       @FormParam("content") String content,
                       @FormParam("shopped") String shopped,
                       @FormParam("eaten") String eaten,
                       @FormParam("otherPlan") String otherPlan
    ){
        Meal meal = mealEndpoint.load(id);
        meal.setContent(content);
        meal.setShopped("on".equals(shopped));
        meal.setEaten("on".equals(eaten));
        meal.setOtherPlan("on".equals(otherPlan));
        mealEndpoint.save(meal);
    }
}
