package com.joofthan.apps.myhome;

import com.joofthan.lib.user.UserService;

import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Root;
import jakarta.transaction.Transactional;
import java.util.List;
import java.util.Objects;
import java.util.Optional;


@Transactional
public abstract class BaseRepository<T extends BaseEntity> {

    private final Class<T> entityType;

    protected BaseRepository(Class<T> entityType){
        this.entityType = entityType;
    }

    @Inject
    protected EntityManager em;

    @Inject
    protected UserService userService;

    public T findById(Long id){
        T result = em.find(entityType, id);
        if(result == null) return null;
        Long userId = userService.getUser().getUserId().id();
        if(result.userId.longValue() != userId.longValue()){
            throw new RuntimeException("Wrong User");
        }
        return result;
    }

    public List<T> findAllForUser(){
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(entityType);
        Root<T> userRoot = criteriaQuery.from(entityType);
        List<T> queryResult = em.createQuery(criteriaQuery.select(userRoot)
                        .where(criteriaBuilder.equal(userRoot.get("userId"), userService.getUser().getUserId().id())))
                .getResultList();
        return queryResult;
    }

    public T merge(T e){
        T existing = null;
        if(e.getId() != null){
            existing = em.find(entityType, e.getId());
        }
        Long userId = userService.getUser().getUserId().id();
        if(existing != null){
            if(existing.userId.longValue() != userId.longValue()){
                throw new RuntimeException("Wrong User");
            }
        }
        if(e.getId() == null && e.getUserId() == null){
            //new Entity
            e.setUserId(userId);
        }
        if(!Objects.equals(e.getUserId(), userId)){
            throw new RuntimeException("Wrong User");
        }
        return em.merge(e);
    }


    public Optional<T> findForUser(){
        List<T> allForUser = findAllForUser();
        if(allForUser.size() == 0){
            return Optional.empty();
        }
        if(allForUser.size() > 1){
            throw new RuntimeException("More than one Entity found for user");
        }
        return Optional.of(allForUser.get(0));
    }
}
