package com.joofthan.apps.mealplanner.cookingapi.backend.boundary.v2023_07_01;

import com.joofthan.apps.mealplanner.cookingapi.Parser;
import com.joofthan.apps.mealplanner.cookingapi.backend.entity.RecipeEntity;
import com.joofthan.apps.mealplanner.cookingapi.backend.entity.IngredientEntity;
import org.openapi.quarkus.spoonacular_json.model.GetRecipeInformation200Response;

import java.math.BigDecimal;
import java.util.List;

public record RecipeDetails(
        //General
        String recipeId,
        String name,
        Integer servings,//Default: 1 //TODO: not in spoonacular
        Integer prepTimeInMinutes, //Optional
        Integer cookTimeInMinutes, //Optional
        Integer totalTimeInMinutes, //Optional
        String author, //Optional
        String imageUrl,
        String sourceUrl, //Optional

        //Details
        List<RecipeDetailIngredient> ingredients,

        //Other
        RecipeSource source,
        String originalDBValue,
        String originalSpoonacularValue
) {
    public static RecipeDetails ofSpoonacularString(long spoonacularId,String recipeDetailsAsString) {
        var json = Parser.stringToObject(recipeDetailsAsString, GetRecipeInformation200Response.class);
        var ingredients = json.getExtendedIngredients().stream().map(e-> new RecipeDetailIngredient(e.getName(), RecipeSource.SPOONACULAR_API, null/*e*/)).toList();
        BigDecimal servingsDecimal = json.getServings();
        return new RecipeDetails(
                "spoonacular-"+spoonacularId,
                json.getTitle(),
                servingsDecimal == null?null:Integer.parseInt(servingsDecimal.toPlainString()),
                null,
                null,
                json.getReadyInMinutes(),
                json.getSourceName(),
                json.getImage(),
                json.getSpoonacularSourceUrl(),
                ingredients,
                RecipeSource.SPOONACULAR_API,
                null,
                null//recipeDetailsAsString
        );
    }

    public static RecipeDetails ofDbEntity(RecipeEntity e) {
        return new RecipeDetails(
                "database-"+e.getId(),
                e.getTitle(),
                 1,
                null,
                null,
                null,
                null,
                e.getImage(),
                null,
                e.getIngredients().stream().map(ess -> mapIngredient(ess)).toList(),
                RecipeSource.FLAVOUR_DB,
                null,
                null
        );
    }

    private static RecipeDetailIngredient mapIngredient(IngredientEntity e) {
        return new RecipeDetailIngredient(
                e.toString(),
                RecipeSource.FLAVOUR_DB,
                null
        );
    }

}
