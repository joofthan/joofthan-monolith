package com.joofthan.apps.mealplanner.cookingapi.sources.foodjson;


import com.joofthan.apps.mealplanner.cookingapi.sources.foodjson.external.v2023_06_29.OriginalSRLegacyIngredientEntity;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.transaction.Transactional;

import java.io.IOException;
import java.util.List;


@ApplicationScoped
public class FoodJsonApi  {

    @Inject
    FoodJsonImporter foodJsonImporter;

    @Inject
    EntityManager em;

    public void doImport() {
        //printDescriptionAndAdd(loadZipFile("survey", survey).get("SurveyFoods"));
        //printDescriptionAndAdd(loadZipFile("legacy", legacy).get("SRLegacyFoods"));
        //printDescriptionAndAdd(loadZipFile("foundation", foundation).get("FoundationFoods"));

        try {
            foodJsonImporter.doImport();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        System.out.println("Hello");


    }

    @Transactional
    public List<OriginalSRLegacyIngredientEntity> search(String text){
        if(searchInternal("Apricots, raw").size() == 0) doImport();

        return searchInternal(text);
    }

    private List<OriginalSRLegacyIngredientEntity> searchInternal(String text) {
        var query = em.createQuery("select e from OriginalSRLegacyIngredientEntity e where description like :text", OriginalSRLegacyIngredientEntity.class);
        query.setParameter("text", "%"+text+"%");
        return query.getResultList();
    }
}
