package com.joofthan.apps.mealplanner.cookingapi.backend;

import com.joofthan.apps.mealplanner.cookingapi.backend.entity.RecipeEntity;

import java.util.List;

public class ExampleData {

    public static RecipeEntity pastaEntity(Long id){
        var e = new RecipeEntity();
        e.setId(id);
        e.setTitle("Test Bolognese");
        e.setImage("http://joofthan.com/images/locked.png");
        e.setIngredients(List.of(
                //new IngredientEntity("Pasta 500g","500g"),
                //new IngredientEntity("Tomato 200g","200g")
        ));
        return e;
    }
}
