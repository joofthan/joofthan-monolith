package com.joofthan.apps.mealplanner.cookingapi.backend.boundary.v2023_07_01;

import com.joofthan.apps.mealplanner.cookingapi.sources.spoonakular.entity.SpoonacularIngredientSearchResult;

public record IngredientSearchResultRow(
        String id,
        String title,
        String image
) {
    public static IngredientSearchResultRow of(SpoonacularIngredientSearchResult e) {
        return new IngredientSearchResultRow(
                String.valueOf(e.id()),
                e.title(),
                e.image()
        );
    }
}