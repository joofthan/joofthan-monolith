package com.joofthan.apps.mealplanner.cookingapi.backend.control;

import com.joofthan.apps.mealplanner.cookingapi.backend.ExampleData;
import com.joofthan.apps.mealplanner.cookingapi.backend.boundary.v2023_07_01.RecipeSearchResultRow;
import com.joofthan.apps.mealplanner.cookingapi.backend.entity.RecipeEntity;
import com.joofthan.lib.Injectable;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;

import java.util.List;

@Injectable
@RequiredArgsConstructor
public class RecipeDBService {

    final RecipeDBRepository recipeDBRepository;

    public List<RecipeSearchResultRow> recipeSearchResult(String query){
        var list = recipeDBRepository.findByTitle(query);
        return list.stream().map(recipeEntity -> RecipeSearchResultRow.of(recipeEntity)).toList();
    }

    @PostConstruct
    void init(){
        var dbe = recipeDBRepository.findById(1L);//TODO: remove
        if (dbe != null){
            recipeDBRepository.deleteById(dbe.getId());
        }
        recipeDBRepository.add(ExampleData.pastaEntity(1L));
    }

    public RecipeEntity byId(String numericId) {
        return recipeDBRepository.findById(Long.parseLong(numericId));
    }
}
