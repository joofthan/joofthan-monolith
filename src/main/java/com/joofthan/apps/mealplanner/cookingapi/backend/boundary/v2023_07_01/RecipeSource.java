package com.joofthan.apps.mealplanner.cookingapi.backend.boundary.v2023_07_01;

public enum RecipeSource {
    FLAVOUR_DB,
    SPOONACULAR_API
}
