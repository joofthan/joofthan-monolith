package com.joofthan.apps.mealplanner.cookingapi.backend.entity;

import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import lombok.*;

import java.util.List;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class RecipeEntity extends PanacheEntityBase {
    @Id
    Long id;
    String title;
    String image;
    boolean external;
    @OneToMany(cascade = CascadeType.PERSIST)
    List<IngredientEntity> ingredients;

    public static List<RecipeEntity> findByTitle(String query) {
        return list("title like ?1", "%" + query + "%");
    }
}
