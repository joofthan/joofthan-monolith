package com.joofthan.apps.mealplanner.cookingapi.backend.boundary.v2023_07_01;

import java.util.List;

public record RecipeSearchResult(
        String info,
        List<RecipeSearchResultRow> recipeSearchResults
) {
}
