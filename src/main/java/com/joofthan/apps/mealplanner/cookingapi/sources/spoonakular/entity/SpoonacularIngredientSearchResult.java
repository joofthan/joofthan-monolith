package com.joofthan.apps.mealplanner.cookingapi.sources.spoonakular.entity;

public record SpoonacularIngredientSearchResult(
        int id,
        String title,
        /**
         * https://spoonacular.com/cdn/ingredients_100x100/
         */
        String image
) {
}
