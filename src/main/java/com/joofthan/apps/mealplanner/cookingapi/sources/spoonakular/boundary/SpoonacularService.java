package com.joofthan.apps.mealplanner.cookingapi.sources.spoonakular.boundary;

import com.joofthan.apps.mealplanner.cookingapi.Parser;
import com.joofthan.apps.mealplanner.cookingapi.backend.boundary.v2023_07_01.IngredientSearchResultRow;
import com.joofthan.apps.mealplanner.cookingapi.backend.boundary.v2023_07_01.RecipeSearchResultRow;
import com.joofthan.apps.mealplanner.cookingapi.sources.spoonakular.control.SpoonacularClient;
import com.joofthan.apps.mealplanner.cookingapi.sources.spoonakular.entity.SpoonacularRecipeSearchResultList;
import com.joofthan.lib.Log;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import org.eclipse.microprofile.rest.client.inject.RestClient;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@ApplicationScoped
public class SpoonacularService {

    final SpoonacularClient spoonacular;

    @Inject
    public SpoonacularService(@RestClient SpoonacularClient spoonacular){
        this.spoonacular = spoonacular;
    }

    Map<String, List<RecipeSearchResultRow>> searchResults = new HashMap<>();
    Map<String, List<IngredientSearchResultRow>> ingredientResults = new HashMap<>();
    Map<String, String> recipeDetails = new HashMap<>();

    public List<RecipeSearchResultRow> getRecipes(String query) {
        if(searchResults.containsKey(query)){
            return searchResults.get(query);
        }

        List<RecipeSearchResultRow> recipes = Parser.stringToObject(spoonacular.searchRecipesAsString(query), SpoonacularRecipeSearchResultList.class).results().stream().map(RecipeSearchResultRow::of).toList();
        searchResults.put(query, recipes);
        Log.memoryLeak(getClass());

        return recipes;
    }

    public List<IngredientSearchResultRow> getIngredients(String query) {
        return this.ingredientResults.computeIfAbsent(query, (k)-> {
                    Log.memoryLeak(getClass());
                    return spoonacular.getIngredient(k).results().stream().map(IngredientSearchResultRow::of).toList();
                }
        );
    }

    public String getRecipeDetailsAsString(String id) {
        return this.recipeDetails.computeIfAbsent(id, (i)-> {
                    Log.memoryLeak(getClass());
                    return spoonacular.getRecipeByIdAsString(i, true);
                }
        );
    }
}
