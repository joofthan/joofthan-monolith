package com.joofthan.apps.mealplanner.cookingapi.backend.control;

import com.joofthan.apps.mealplanner.cookingapi.backend.entity.IngredientEntity;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;

@ApplicationScoped
class IngredientEntityRepository {


    @Inject
    EntityManager em;


    public void add(IngredientEntity ingredientEntity){
        em.merge(ingredientEntity);
    }
}
