package com.joofthan.apps.mealplanner.cookingapi.sources.spoonakular.entity;

public record SpoonacularRecipeSearchResult(
        int id,
        //Fetch details with id from api
        @Deprecated
        String title,
        @Deprecated
        String image,
        @Deprecated
        String imageType,
        @Deprecated
        int readyInMinutes,
        @Deprecated
        String summary

) {
}
