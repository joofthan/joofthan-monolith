package com.joofthan.apps.mealplanner.cookingapi.sources.spoonakular.entity;

import java.util.List;

public record SpoonacularIngredientSearchResultList(
        List<SpoonacularIngredientSearchResult> results
) {
}
