package com.joofthan.apps.mealplanner.cookingapi.sources.team_recipes;


import com.joofthan.apps.mealplanner.cookingapi.backend.entity.IngredientEntity;
import com.joofthan.apps.mealplanner.cookingapi.backend.entity.RecipeEntity;
import lombok.AllArgsConstructor;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@AllArgsConstructor
public class TeamRecipeData {

    final RemoteCsvLoader remoteCsvLoader;


    public List<RecipeEntity> fetchAsEntitiesFromRemote() throws Exception {
        var rezepte = remoteCsvLoader.rezepte();
        var rezepte_zutaten = remoteCsvLoader.rezepte_zutaten();
        var zutaten = remoteCsvLoader.zutaten();

        var alleRezeptEntities = new ArrayList<RecipeEntity>();


        for (int i = 1; i < rezepte.size(); i++) {
            String[] r = Arrays.stream(rezepte.get(i)).map(feld->{
                if(feld.equals("null")){
                    return null;
                }
                return feld;
            }).toArray(String[]::new);


            String recipeId = r[0];
            String title = r[1];
            String category = r[2];
            String calories = r[3];
            String rating = r[4];
            String difficulty = r[5];
            String prepTimeInMinutes = r[6];
            String cookTImeInMinutes = r[7];
            String isFavorite = r[8];
            String author = r[9];
            String imageUrl = r[10];


           alleRezeptEntities.add(new RecipeEntity(
                   Long.parseLong(recipeId),
                   title,
                   imageUrl,
                   false,
                   parseIngredients(recipeId, rezepte_zutaten, zutaten)
                   )
           );
        }

        return alleRezeptEntities;

    }

    private List<IngredientEntity> parseIngredients(String recipeIdParam, List<String[]> rezepte_zutaten, List<String[]> zutaten) {
        var list = new ArrayList<IngredientEntity>();

        for (int i = 1; i < rezepte_zutaten.size(); i++) {
            String[] zutaten_rezept_kreuztabelle = rezepte_zutaten.get(i);
            String recipeId = zutaten_rezept_kreuztabelle[0];
            String ingredientId = zutaten_rezept_kreuztabelle[1];
            String amount = zutaten_rezept_kreuztabelle[2];
            String unit = zutaten_rezept_kreuztabelle[3];

            String ingredientName = findIngredient(ingredientId, zutaten);
            list.add(new IngredientEntity(

            ));
        }

        return list;
    }

    private String findIngredient(String ingredientIdParam, List<String[]> zutaten) {
        for (int i = 1; i < zutaten.size(); i++) {
            String[] z = zutaten.get(i);
            String name = z[1];
            String imageUrl = z[2];
            if(ingredientIdParam.equals(z[0])){
                return name;
            }
        }

        throw new RuntimeException("Zutat nicht gefunden: "+ingredientIdParam);
    }

    public static String downloadString(String url) throws IOException {
        StringBuilder s = new StringBuilder();
        BufferedReader reader = new BufferedReader(new InputStreamReader(new URL(url).openStream()));
        String ss;
        while ((ss = reader.readLine()) != null){
            s.append(ss);
        }
        return s.toString();
    }

    public List<IngredientEntity> findAllIngredients() {
        var list = new ArrayList<IngredientEntity>();
        List<String[]> zutaten = remoteCsvLoader.zutaten();
        String[] header = zutaten.get(0);
        for (int i = 1; i < zutaten.size(); i++) {
            list.add(IngredientEntity.ofCsvLine(CsvLine.of(zutaten.get(i), header)));
        }

        return list;
    }
}
