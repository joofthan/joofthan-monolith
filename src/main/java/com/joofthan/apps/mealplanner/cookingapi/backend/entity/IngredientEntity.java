package com.joofthan.apps.mealplanner.cookingapi.backend.entity;

import com.joofthan.apps.mealplanner.cookingapi.sources.team_recipes.CsvLine;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.ToString;

@ToString
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class IngredientEntity {
    @Id
    Long id;

    //Ingredient Table
    String name;
    String imageUrl;
    boolean isSeasoning;

    //RecipeIngredient Cross Table
    String unit;
    Integer amount = 100;

    //Nutrition Table
    Integer calories;
    Float protein;
    Float carbohydrates;
    Float fat;

    public static IngredientEntity ofCsvLine(CsvLine c) {
        return new IngredientEntity(
                Long.parseLong(c.getByName("id")),
                c.getByName("name"),
                c.getByName("imageUrl"),
                Boolean.parseBoolean(c.getByName("isSeasoning")),
                "todoUnit",
                null,
                null,
                null,
                null,
                null
        );
    }
}