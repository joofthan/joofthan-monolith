package com.joofthan.apps.mealplanner.cookingapi.sources.foodjson;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.joofthan.apps.mealplanner.cookingapi.sources.foodjson.external.v2023_06_29.OriginalSRLegacyIngredientEntity;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.transaction.Transactional;

import java.io.*;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

@ApplicationScoped
class FoodJsonImporter {

    //TODO: Inject
    String foundation = "https://fdc.nal.usda.gov/fdc-datasets/FoodData_Central_foundation_food_json_2022-10-28.zip";
    String survey = "https://fdc.nal.usda.gov/fdc-datasets/FoodData_Central_survey_food_json_2022-10-28.zip";
    String legacy = "https://fdc.nal.usda.gov/fdc-datasets/FoodData_Central_sr_legacy_food_json_2021-10-28.zip";

    @Inject
    EntityManager em;

    @Transactional
    public void doImport() throws IOException {
        var list = loadZipFile("legacy", legacy).get("SRLegacyFoods").getAsJsonArray();
        list.forEach(e-> {
            String description = e.getAsJsonObject().get("description").getAsString();
            if(!e.isJsonNull()){
                String content = e.toString();
                OriginalSRLegacyIngredientEntity entity = new OriginalSRLegacyIngredientEntity();
                entity.setDescription(description);
                entity.setOriginalContent(content);
                em.merge(entity);
            }else {
                System.out.println("Not Importet: "+description);
            }
        });
    }

    private void printDescriptionAndAdd(JsonElement fou) {

    }

    private JsonObject loadZipFile(String filenam, String url) throws IOException {
        Path myPAth = Files.createTempFile(null, null);
        //Path myPAth = Paths.get("target/"+filenam+".zip");//TODO: target in db saven
        if(!myPAth.toFile().exists()){
            InputStream in = new URL(url).openStream();
            Files.copy(in, myPAth, StandardCopyOption.REPLACE_EXISTING);
        }
        File tempDirWithPrefix = Files.createTempDirectory(filenam).toFile();
        File destDir = new File("target/unzipTest/"+filenam);//TODO // in db unzippen
        if(!destDir.exists()) extractZipFile(myPAth, destDir);
        String res = readFromInputStream(new FileInputStream(destDir.listFiles()[0]));
        var json = new JsonParser().parse(res);
        var fo = json.getAsJsonObject();
        return fo;
    }


    private String readFromInputStream(InputStream inputStream)
            throws IOException {
        StringBuilder resultStringBuilder = new StringBuilder();
        try (BufferedReader br
                     = new BufferedReader(new InputStreamReader(inputStream))) {
            String line;
            while ((line = br.readLine()) != null) {
                resultStringBuilder.append(line).append("\n");
            }
        }
        return resultStringBuilder.toString();
    }

    private static void extractZipFile(Path foundationZip, File destDir) throws IOException {
        byte[] buffer = new byte[1024];
        ZipInputStream zis = new ZipInputStream(new FileInputStream(foundationZip.toFile()));
        ZipEntry zipEntry = zis.getNextEntry();
        while (zipEntry != null) {
            File newFile = newFile(destDir, zipEntry);
            if (zipEntry.isDirectory()) {
                if (!newFile.isDirectory() && !newFile.mkdirs()) {
                    throw new IOException("Failed to create directory " + newFile);
                }
            } else {
                // fix for Windows-created archives
                File parent = newFile.getParentFile();
                if (!parent.isDirectory() && !parent.mkdirs()) {
                    throw new IOException("Failed to create directory " + parent);
                }

                // write file content
                FileOutputStream fos = new FileOutputStream(newFile);
                int len;
                while ((len = zis.read(buffer)) > 0) {
                    fos.write(buffer, 0, len);
                }
                fos.close();
            }
            zipEntry = zis.getNextEntry();
        }

        zis.closeEntry();
        zis.close();
    }


    public static File newFile(File destinationDir, ZipEntry zipEntry) throws IOException {
        File destFile = new File(destinationDir, zipEntry.getName());

        String destDirPath = destinationDir.getCanonicalPath();
        String destFilePath = destFile.getCanonicalPath();

        if (!destFilePath.startsWith(destDirPath + File.separator)) {
            throw new IOException("Entry is outside of the target dir: " + zipEntry.getName());
        }

        return destFile;
    }

}
