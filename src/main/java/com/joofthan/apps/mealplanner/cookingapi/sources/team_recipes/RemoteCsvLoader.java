package com.joofthan.apps.mealplanner.cookingapi.sources.team_recipes;

import com.opencsv.CSVReader;
import jakarta.enterprise.context.ApplicationScoped;
import org.eclipse.microprofile.config.ConfigProvider;

import java.io.InputStreamReader;
import java.net.URL;
import java.util.List;

@ApplicationScoped
class RemoteCsvLoader {

    private List<String[]> loader(String rezepteZutaten) throws Exception {
        try (CSVReader reader = new CSVReader(new InputStreamReader(new URL(ConfigProvider.getConfig().getValue("joofthan.teamrecipes.testdata.url", String.class) +rezepteZutaten+".csv?inline=false").openStream()))) {
            return reader.readAll();
        }
    }

    public List<String[]> rezepte() {
        try {
            return loader("rezepte");
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public List<String[]> rezepte_zutaten() {
        try {
            return loader("rezepte_zutaten");
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public List<String[]> zutaten() {
        try {
            return loader("zutaten");
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
