package com.joofthan.apps.mealplanner.cookingapi.sources.team_recipes;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class CsvLine {

    private final String[] csvLine;
    private final Map<String, Integer> headerIndexMapping = new HashMap<>();

    public CsvLine(String[] csvLine, String[] headers){
        this.csvLine = csvLine;
        for (int i = 0; i < headers.length; i++) {
            this.headerIndexMapping.put(headers[i], i);
        }
    }

    public static CsvLine of(String[] strings, String[] header) {
        return new CsvLine(strings,header);
    }

    public String getByName(String name){
        if(!headerIndexMapping.containsKey(name)){
            throw new IllegalArgumentException("\""+name + "\" not found in "+headerIndexMapping + " ("+ Arrays.toString(csvLine) +")");
        }
        String value = csvLine[headerIndexMapping.get(name)];
        if(value==null)value="";
        return value;
    }
}
