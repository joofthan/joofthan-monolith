package com.joofthan.apps.mealplanner.cookingapi.backend.boundary.v2023_07_01;

import com.joofthan.apps.mealplanner.cookingapi.backend.control.RecipeDBService;
import com.joofthan.apps.mealplanner.cookingapi.sources.foodjson.FoodJsonApi;
import com.joofthan.apps.mealplanner.cookingapi.sources.spoonakular.boundary.SpoonacularService;
import com.joofthan.lib.Injectable;
import lombok.RequiredArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Injectable
@RequiredArgsConstructor
public class CookingApiService {

    final SpoonacularService spoonacularService;
    final RecipeDBService recipeDBService;
    final FoodJsonApi foodJsonApi;

    public RecipeSearchResult searchRecipeByName(String name) {
        List<RecipeSearchResultRow> list = new ArrayList<>();
        list.addAll(recipeDBService.recipeSearchResult(name));
        String message = null;
        try{
            list.addAll(spoonacularService.getRecipes(name));
        }catch (Exception e){
            message = "Spoonacular not available: " + e.getMessage();
            e.printStackTrace();
        }

        return new RecipeSearchResult(
                message,
                list
        );
    }

    public List<IngredientSearchResultRow> searchIngredient(String name) {
        return foodJsonApi.search(name).stream().map(e->{
            return new IngredientSearchResultRow("foodjson-"+e.getId(),e.getDescription(),null);
        }).toList();
        //return spoonacularService.getIngredients(name);
    }

    public RecipeDetails getRecipById(String id) {
        if(id.startsWith("spoonacular-")) {
            String numericId = id.replace("spoonacular-", "");
            String recipeDetailsAsString = spoonacularService.getRecipeDetailsAsString(numericId);
            return RecipeDetails.ofSpoonacularString(Long.parseLong(numericId), recipeDetailsAsString);
        }
        if(id.startsWith("database-")) {
            String numericId = id.replace("database-", "");
            var recipeFromDb = recipeDBService.byId(numericId);
            return RecipeDetails.ofDbEntity(recipeFromDb);
        }
        else throw new UnsupportedOperationException("Id not recognized: "+id);
    }

}
