package com.joofthan.apps.mealplanner.cookingapi.backend.control;

import com.joofthan.apps.mealplanner.cookingapi.backend.entity.RecipeEntity;
import com.joofthan.lib.Injectable;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;

import java.util.List;

@Injectable
public class RecipeDBRepository {

    @Inject
    EntityManager em;


    public List<RecipeEntity> findByTitle(String query) {
        return RecipeEntity.findByTitle(query);
    }

    public RecipeEntity findById(long id) {
        return em.find(RecipeEntity.class,id);
    }

    public void deleteById(Long id) {
        var e = findById(id);
        e.delete();
    }

    public void add(RecipeEntity e) {
        e.persist();
    }
}
