package com.joofthan.apps.mealplanner.cookingapi;

import jakarta.json.Json;
import jakarta.json.bind.Jsonb;
import jakarta.json.bind.JsonbBuilder;
import jakarta.json.stream.JsonParser;

import java.io.StringReader;

public class Parser {
    public static Jsonb jsonb = JsonbBuilder.create();

    public static <T> T stringToObject(String recipesAsString, Class<T> clazz) {
        //JsonParser json = Json.createParser(new StringReader(recipesAsString));
        if(recipesAsString == null) return null;
        return (T) jsonb.fromJson(recipesAsString,clazz);
    }

    public static JsonParser toJsonObject(String jsonString) {
        JsonParser parser = Json.createParser(new StringReader(jsonString));
        return parser;
    }

    public static String toJsonString(Object e) {
        return jsonb.toJson(e);
    }
}
