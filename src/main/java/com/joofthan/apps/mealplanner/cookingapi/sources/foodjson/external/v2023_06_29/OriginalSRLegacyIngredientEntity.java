package com.joofthan.apps.mealplanner.cookingapi.sources.foodjson.external.v2023_06_29;


import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.Lob;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
public class OriginalSRLegacyIngredientEntity {
    @Id
    @GeneratedValue
    Long id;
    String description;
    @Lob
    String originalContent;
}
