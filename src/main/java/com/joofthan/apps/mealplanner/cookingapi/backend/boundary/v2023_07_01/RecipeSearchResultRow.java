package com.joofthan.apps.mealplanner.cookingapi.backend.boundary.v2023_07_01;

import com.joofthan.apps.mealplanner.cookingapi.backend.entity.RecipeEntity;
import com.joofthan.apps.mealplanner.cookingapi.sources.spoonakular.entity.SpoonacularRecipeSearchResult;

public record RecipeSearchResultRow(
        String id,
        String title,
        String image
) {
    public static RecipeSearchResultRow of(SpoonacularRecipeSearchResult e) {
        return new RecipeSearchResultRow(
                "spoonacular-" + e.id(),
                e.title(),
                e.image()
        );
    }

    public static RecipeSearchResultRow of(RecipeEntity e) {
        return new RecipeSearchResultRow(
                "database-"+e.getId(),
                e.getTitle(),
                e.getImage()
        );
    }
}