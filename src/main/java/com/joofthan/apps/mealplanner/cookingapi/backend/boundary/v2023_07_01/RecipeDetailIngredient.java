package com.joofthan.apps.mealplanner.cookingapi.backend.boundary.v2023_07_01;

//TODO: Fetch from food.json
public record RecipeDetailIngredient(
        String text,
        RecipeSource source,
        org.openapi.quarkus.spoonacular_json.model.GetRecipeInformation200ResponseExtendedIngredientsInner originalSpoonacularValue
) {

}
