package com.joofthan.apps.mealplanner.cookingapi.sources.spoonakular.control;

import com.joofthan.apps.mealplanner.cookingapi.sources.spoonakular.entity.SpoonacularIngredientSearchResultList;
import com.joofthan.apps.mealplanner.cookingapi.sources.spoonakular.entity.SpoonacularRecipeSearchResultList;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.QueryParam;
import org.eclipse.microprofile.config.ConfigProvider;
import org.eclipse.microprofile.rest.client.annotation.ClientHeaderParam;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

@RegisterRestClient(configKey="spoonacular")
@ClientHeaderParam(name = "x-api-key", value = "{getToken}")
public interface SpoonacularClient {

    default String getToken(){
        return ConfigProvider.getConfig().getValue("spoonacular.key", String.class);
    }

    @GET
    @Path("recipes/complexSearch")
    String searchRecipesAsString(@QueryParam("query") String name);

    @Deprecated
    @GET
    @Path("recipes/complexSearch")
    SpoonacularRecipeSearchResultList searchRecipes(@QueryParam("query") String name);

    @GET
    @Path("food/ingredients/search")
    String getIngredientAsString(@QueryParam("query") String name);

    @Deprecated
    @GET
    @Path("food/ingredients/search")
    SpoonacularIngredientSearchResultList getIngredient(@QueryParam("query") String name);

    @GET
    @Path("recipes/{id}/information")
    String getRecipeByIdAsString(@PathParam("id") String id, @QueryParam("includeNutrition") boolean includeNutrition);

}
