package com.joofthan.apps.mealplanner.backend;

import com.joofthan.apps.mealplanner.backend.food.FoodserviceEndpoint;
import com.joofthan.apps.mealplanner.backend.food.MealRequest;
import com.joofthan.apps.mealplanner.backend.food.MealResponse;
import com.joofthan.apps.mealplanner.backend.recipes.v1.AppRecipesApiEndpoint;
import com.joofthan.apps.mealplanner.backend.shoplist.ShoppingItemEntity;
import com.joofthan.apps.mealplanner.backend.shoplist.ShoppingItemsEndpoint;
import com.joofthan.apps.mealplanner.cookingapi.sources.spoonakular.boundary.SpoonacularService;
import com.joofthan.lib.TransactionalBoundary;
import jakarta.inject.Inject;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import lombok.RequiredArgsConstructor;

import java.util.List;

@Produces(MediaType.APPLICATION_JSON)
@TransactionalBoundary
@Path("/mealplanner/api")
@RequiredArgsConstructor
public class MealPlannerEndpoint {


    final ShoppingItemsEndpoint shoppingEndpoint;

    //
    //   Shopping List
    //

    @GET
    @Path("/shopping/items")
    public List<ShoppingItemEntity> getShoppingItems(){
        return shoppingEndpoint.getShoppingItems();
    }

    @GET
    @Path("/shopping/item/{id}")
    public Response getShoppingItemById(@PathParam("id") String id){
        return shoppingEndpoint.getShoppingItemById(id);
    }

    @PUT
    @Path("/shopping/item")
    public Response addShoppingItem(ShoppingItemEntity shoppingItem){
       return shoppingEndpoint.addShoppingItem(shoppingItem);
    }

    @PUT
    @Path("/shopping/item/{id}")
    public Response updateShoppingItemById(@PathParam("id")String id, ShoppingItemEntity shoppingItem){
        return shoppingEndpoint.updateShoppingItemById(id,shoppingItem);
    }

    @DELETE
    @Path("/shopping/item/{id}")
    public Response deleteShoppingItemById(@PathParam("id") String id){
        return shoppingEndpoint.deleteShoppingItemById(id);
    }

    @PATCH
    @Path("/shopping/items/")
    public Response deleteShoppingItemsByIds(List<String> ids){
        return shoppingEndpoint.deleteShoppingItemsByIds(ids);
    }


    //
    // Meal
    //

    @Inject
    FoodserviceEndpoint foodService;

    @GET
    @Path("calendar/meal")
    public List<MealResponse> getMeals(){
        return foodService.getMeals();
    }

    @GET
    @Path("calendar/meal/{id}")
    public MealResponse getMealById(@PathParam("id") String id){
        return foodService.getMealById(id);
    }

    @PUT
    @Path("calendar/meal/{id}")
    public void putMealById(@PathParam("id")String id, MealRequest meal){
        foodService.putMealById(id,meal);
    }

    @DELETE
    @Path("calendar/meal/{id}")
    public void deleteMealById(@PathParam("id") String id){
        foodService.deleteMealById(id);
    }


    //
    // Recipes
    //

    final AppRecipesApiEndpoint appRecipesApiEndpoint;

    @Deprecated
    @GET
    @Path("cookbook/recipes")
    public Response getRecipeById() {
        return null;
    }

    @GET
    @Path("cookbook/recipes/{id}")
    public Response getRecipeById(@PathParam("id") String id) {
        return appRecipesApiEndpoint.getRecipeById(id);
    }

    @GET
    @Path("cookbook/summaries")
    public Response searchRecipes(@QueryParam("title") String title){
        return appRecipesApiEndpoint.getRecipes(title);
    }

    @GET
    @Path("cookbook/summaries/info")
    public Response searchRecipesWithInfo(@QueryParam("title") String title){
        return appRecipesApiEndpoint.getRecipes(title);
    }

    @Deprecated
    @GET
    @Path("cookbook/favorites")
    public Response getFavos() {
        return null;
    }


    @GET
    @Path("ingredients")
    public Response getIngredients(@QueryParam("title") String title) {
        return appRecipesApiEndpoint.getIngredients(title);
    }

    final SpoonacularService spoonacularService;

    @GET
    @Deprecated
    @Path("public/recipes/search")
    public Response getPublicRecipes(@QueryParam("title") String title){
        return Response.ok(spoonacularService.getRecipes(title)).build();
    }

    @GET
    @Deprecated
    @Path("public/recipe/{id}")
    public Response getPublicRecipe(@QueryParam("id") String id){
        return Response.ok(spoonacularService.getRecipeDetailsAsString(id)).build();
    }
}
