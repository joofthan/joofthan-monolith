package com.joofthan.apps.mealplanner.backend.food;

import java.time.OffsetDateTime;

public record MealResponse(
        String mealId,                    // Id, die beim Anlegen der Mahlzeit angegeben werden muss. Darüber kann der Nutzer seine Mahlzeiten wieder finden
        String title,                     // Titel der Mahlzeit. Freitext Eingabe des Nutzers
        MealTimeLabeling mealTimeLabeling,// BREAKFAST, LUNCH, DINNER, SNACK, UNKNOWN
        OffsetDateTime createdAt,         // Zeitpunkt zu dem der Mahlzeiteneintrag angelegt wurde , Format: "YYYY-MM-DDTHH:MM:SS-ZZ:ZZ"
        OffsetDateTime startedAt,         // Zeitpunkt zu dem die Mahlzeit gegessen wird , Format: "YYYY-MM-DDTHH:MM:SS-ZZ:ZZ"
        OffsetDateTime finishedAt,        // Optional; Zeitpunkt zu dem die Mahlzeit fertig gegessen wurde , Format: "YYYY-MM-DDTHH:MM:SS-ZZ:ZZ"
        Boolean isLeftover                // ist true wenn die Mahlzeit schon vorbereitet wurde, und nicht mehr gekocht werden muss.
) {
    public static MealResponse of(MealEntity meal) {
        return new MealResponse(
                meal.getMealId(),
                meal.getTitle(),
                meal.getMealTimeLabeling(),
                meal.getCreatedAt(),
                meal.getStartedAt(),
                meal.getFinishedAt(),
                meal.getIsLeftover()
        );
    }
}
