package com.joofthan.apps.mealplanner.backend.shoplist;


import com.joofthan.lib.Injectable;
import com.joofthan.lib.Log;
import com.joofthan.lib.user.UserService;
import lombok.RequiredArgsConstructor;

import java.util.List;
import java.util.Optional;

@Injectable
@RequiredArgsConstructor
public class ShoppingItemRepository {
    final UserService userService;

    public List<ShoppingItemEntity> findAll() {
        return ShoppingItemEntity.findByUserId(userService.getUser().getUserId());
    }

    public Optional<ShoppingItemEntity> findByIdForCurrentUser(String id) {
        return findAll().stream().filter(e -> e.id.equals(id)).findFirst();
    }

    public void update(ShoppingItemEntity shoppingItem) {
        var item = findByIdForCurrentUser(shoppingItem.id);
        if(item.isPresent()){
            delete(shoppingItem.id);
        }
        shoppingItem.setUserId(userService.getUser().getUserId().id());
        if(ShoppingItemEntity.findById(shoppingItem.id) != null){
            Log.info("Update failed because of permission: " + shoppingItem.id);
            return;
        }
        shoppingItem.persist();
    }

    public void delete(String id) {
        Optional<ShoppingItemEntity> item = findByIdForCurrentUser(id);
        if(item.isPresent()){
            item.get().delete();
        }else {
            throw new RuntimeException("Item does not exist or no permission");
        }
    }
}
