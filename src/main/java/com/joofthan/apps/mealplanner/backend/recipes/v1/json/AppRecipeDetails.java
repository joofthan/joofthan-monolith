package com.joofthan.apps.mealplanner.backend.recipes.v1.json;

import java.util.List;

public record AppRecipeDetails(
        //General
        String recipeId,
        String name,
        int servings,//Default: 1
        Integer prepTimeInMinutes,
        Integer cookTimeInMinutes,
        Integer totalTimeInMinutes,
        boolean isFavorite,
        String author,
        String imageUrl,
        String sourceUrl,

        //Details
        List<AppIngredient> ingredients
        )
{}
