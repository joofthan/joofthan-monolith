package com.joofthan.apps.mealplanner.backend.shoplist;

import com.joofthan.lib.Injectable;
import lombok.RequiredArgsConstructor;

import java.util.List;
import java.util.Optional;

@Injectable
@RequiredArgsConstructor
public class ShoppingList {

    final ShoppingItemRepository shoppingItemRepository;
    final CategoryEnricher categoryEnricher;

    public List<ShoppingItemEntity> findAll(){
        return shoppingItemRepository.findAll();
    }

    public Optional<ShoppingItemEntity> findShoppingItemById(String id){
        return shoppingItemRepository.findByIdForCurrentUser(id);
    }

    public void updateShoppingItemById(ShoppingItemEntity shoppingItem){
        if(shoppingItem.category == null || shoppingItem.category == Category.UNKNOWN){
            shoppingItem.category = categoryEnricher.fetchCategory(shoppingItem.title);
        }
        shoppingItemRepository.update(shoppingItem);
    }

    public void deleteShoppingItemById(String id){
        shoppingItemRepository.delete(id);
    }

}
