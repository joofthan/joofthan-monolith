package com.joofthan.apps.mealplanner.backend.recipes.v1;

import com.joofthan.apps.mealplanner.cookingapi.backend.boundary.v2023_07_01.CookingApiService;
import com.joofthan.lib.user.UserService;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.ws.rs.core.Response;
import lombok.RequiredArgsConstructor;


@ApplicationScoped
@RequiredArgsConstructor
public class AppRecipesApiEndpoint {
    final CookingApiService cookingApiService;
    final UserService userService;

    public Response getRecipes(String title){
        if(userService.isLoggedIn())return Response.ok(cookingApiService.searchRecipeByName(title).recipeSearchResults()).build();
        return Response.status(404).build();
    }
    public Response getRecipesWithInfo(String title){
        if(userService.isLoggedIn())return Response.ok(cookingApiService.searchRecipeByName(title)).build();
        return Response.status(404).build();
    }

    public Response getIngredients(String title) {
        if(userService.isLoggedIn())return Response.ok(cookingApiService.searchIngredient(title)).build();
        return Response.status(404).build();
    }

    public Response getPublicRecipes(String title) {
        return Response.ok(cookingApiService.searchRecipeByName(title)).build();
    }

    public Response getPublicRecepById(String id) {
        return Response.ok(cookingApiService.getRecipById(id)).build();
    }

    public Response getRecipeById(String id) {
        if(id.startsWith("user-")){
            throw new UnsupportedOperationException("userRecipes not implemented yet");
        }else return Response.ok(cookingApiService.getRecipById(id)).build();
    }
}
