package com.joofthan.apps.mealplanner.backend.food;

import com.joofthan.lib.user.UserId;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.OffsetDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class MealEntity{
        String mealId;// Id, die beim Anlegen der Mahlzeit angegeben werden muss. Darüber kann der Nutzer seine Mahlzeiten wieder finden
        String title;                     // Titel der Mahlzeit. Freitext Eingabe des Nutzers
        MealTimeLabeling mealTimeLabeling;// BREAKFAST, LUNCH, DINNER, SNACK, UNKNOWN
        OffsetDateTime createdAt;// Zeitpunkt zu dem der Mahlzeiteneintrag angelegt wurde , Format: "YYYY-MM-DDTHH:MM:SS-ZZ:ZZ"
        OffsetDateTime startedAt;// Zeitpunkt zu dem die Mahlzeit gegessen wird , Format: "YYYY-MM-DDTHH:MM:SS-ZZ:ZZ"
        OffsetDateTime finishedAt;// Optional; Zeitpunkt zu dem die Mahlzeit fertig gegessen wurde , Format: "YYYY-MM-DDTHH:MM:SS-ZZ:ZZ"
        Boolean isLeftover;// ist true wenn die Mahlzeit schon vorbereitet wurde, und nicht mehr gekocht werden muss.

        Long userId; //ID vom user, der diese mahlzeit bearbeiten darf.

        public static MealEntity of(String id, MealRequest meal, UserId userId) {
                return new MealEntity(
                        id,
                        meal.title(),
                        meal.mealTimeLabeling(),
                        meal.createdAt(),
                        meal.startedAt(),
                        meal.finishedAt(),
                        meal.isLeftover(),
                        userId.id()
                );
        }
}
