package com.joofthan.apps.mealplanner.backend.recipes.v1.json;

/**
 *
 * @param ingredientId
 * @param name
 * @param quantity
 * @param unit
 * @param imageUrl
 * @param rawInput
 */
public record AppIngredient(
        Integer ingredientId,
        String name,
        Float quantity,
        String unit, //TODO: TypeSafe from Meal input parser
        String imageUrl,

        //Other
        String rawInput
) {}
