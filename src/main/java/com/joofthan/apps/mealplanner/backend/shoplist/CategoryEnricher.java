package com.joofthan.apps.mealplanner.backend.shoplist;

import com.joofthan.lib.Injectable;

import static com.joofthan.apps.mealplanner.backend.shoplist.Category.*;

@Injectable
public class CategoryEnricher {

    //TODO: Fetch from api
    public Category fetchCategory(String title){
        if("Apple".equals(title)){
            return FRUIT;
        }else if("abcdefg".equals(title)) {
            return UNKNOWN;
        }else {
            return DEMO_CATEGORY;
        }
    }
}
