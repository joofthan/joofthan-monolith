package com.joofthan.apps.mealplanner.backend.shoplist;

import com.joofthan.lib.Injectable;
import com.joofthan.lib.Log;
import jakarta.ws.rs.core.Response;
import lombok.RequiredArgsConstructor;

import java.util.List;

@Injectable
@RequiredArgsConstructor
public class ShoppingItemsEndpoint {

    final ShoppingList shoppingList;

    public List<ShoppingItemEntity> getShoppingItems(){
        Log.info("getAll");
        return shoppingList.findAll();
    }

    public Response getShoppingItemById(String id){
        Log.info("get: "+id);
        return Response.ok().entity(shoppingList.findShoppingItemById(id).orElseThrow()).build();
    }

    public Response addShoppingItem(ShoppingItemEntity shoppingItem){
        Log.info("add: "+shoppingItem.id);
        shoppingList.updateShoppingItemById(shoppingItem);
        return Response.ok().build();
    }

    public Response updateShoppingItemById(String id, ShoppingItemEntity shoppingItem){
        Log.info("update: "+id);
        shoppingItem.setId(id);
        shoppingList.updateShoppingItemById(shoppingItem);
        return Response.ok().build();
    }

    public Response deleteShoppingItemById(String id){
        Log.info("delete: "+id);
        shoppingList.deleteShoppingItemById(id);
        return Response.ok().build();
    }

    public Response deleteShoppingItemsByIds(List<String> ids){
        Log.info("delete: "+ids);
        Log.slowQuery(getClass());
        ids.forEach(id->{
            shoppingList.deleteShoppingItemById(id);
        });
        return Response.ok().build();
    }

}
