package com.joofthan.apps.mealplanner.backend.shoplist;

import com.joofthan.lib.user.UserId;
import io.quarkus.hibernate.orm.panache.PanacheEntityBase;
import jakarta.persistence.*;
import lombok.*;

import java.util.List;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@Table(name = "SHOPPINGITEM")
public class ShoppingItemEntity extends PanacheEntityBase {
    @Id
    String id;
    /*Private*/ Long userId;
    String title;
    String unit;
    Float amount;
    Boolean isMarked;
    @Enumerated(EnumType.STRING)
    Category category;
    String rawTextInput;
    Long createdAt;

    public static List<ShoppingItemEntity> findByUserId(UserId userId) {
        return list("userId",userId.id());
    }
}
