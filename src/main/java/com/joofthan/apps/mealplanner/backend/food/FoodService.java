package com.joofthan.apps.mealplanner.backend.food;

import com.joofthan.lib.user.UserService;
import jakarta.annotation.PostConstruct;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.ws.rs.NotFoundException;

import java.time.OffsetDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@ApplicationScoped
public class FoodService {

    @Inject
    UserService userService;

    Map<String,MealEntity> meals;

    @PostConstruct
    public void checkLogin(){
        if(!userService.isLoggedIn()){
            throw new UnsupportedOperationException("Required User-Token is missing in Header. Get from \"/login/api/token\". See: /q/openapi");
        }
        meals = new HashMap<>();
        meals.put("1",new MealEntity(
                "1",
                "My first meal",
                MealTimeLabeling.BREAKFAST,
                OffsetDateTime.now(),
                OffsetDateTime.now(),
                null,
                false,
                userService.getUser().getUserId().id()

        ));
        meals.put("2",new MealEntity(
                "2",
                "My second meal (Warning global data)",
                MealTimeLabeling.BREAKFAST,
                OffsetDateTime.now(),
                OffsetDateTime.now(),
                null,
                false,
                userService.getUser().getUserId().id()

        ));
    }

    public List<MealResponse> getMeals() {
        return meals.values().stream().map(MealResponse::of).toList();
    }

    public MealResponse getMealById(String id) {
        if(meals.containsKey(id)){
            if (!meals.get(id).getUserId().equals(userService.getUser().getUserId().id())){
                throw new NotFoundException();//Not Allowed
            }
            return MealResponse.of(meals.get(id));
        }
        throw new NotFoundException();
    }

    public void deleteById(String id) {
        if(meals.containsKey(id)){
            if (!meals.get(id).getUserId().equals(userService.getUser().getUserId().id())){
                throw new NotFoundException();//Not Allowed
            }
            meals.remove(id);
        }
    }

    public void createOrUpdateUserMeal(String id, MealRequest meal) {
        if(meals.containsKey(id)){
            if (!meals.get(id).getUserId().equals(userService.getUser().getUserId().id())){
                throw new NotFoundException();//Not Allowed
            }
        }
        meals.put(id,MealEntity.of(id,meal, userService.getUser().getUserId()));
    }
}
