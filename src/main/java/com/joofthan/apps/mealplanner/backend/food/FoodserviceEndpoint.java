package com.joofthan.apps.mealplanner.backend.food;

import com.joofthan.lib.Injectable;
import jakarta.inject.Inject;

import java.util.List;

@Injectable
public class FoodserviceEndpoint {
    @Inject
    FoodService foodService;

    public List<MealResponse> getMeals(){
        return foodService.getMeals();
    }

    public MealResponse getMealById(String id){
        return foodService.getMealById(id);
    }

    public void putMealById(String id, MealRequest meal){
        foodService.createOrUpdateUserMeal(id,meal);
    }

    public void deleteMealById(String id){
        foodService.deleteById(id);
    }

}
