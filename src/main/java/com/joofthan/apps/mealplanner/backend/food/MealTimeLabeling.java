package com.joofthan.apps.mealplanner.backend.food;

public enum MealTimeLabeling {
    BREAKFAST,
    BRUNCH,
    LUNCH,
    DINNER,
    SNACK,
    UNKNOWN
}
