package com.joofthan.apps.login.entity;

import jakarta.persistence.*;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;


@Entity
@Table(name = "USERDB")
public class UserDB {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;
    @Column(name = "USERNAME")
    private String username;
    @Column(name = "PASSWORDHASH")
    private String passwordHash;
    @Column(name = "LOGINRETRY")
    private Integer loginRetry;
    @Column(name = "LASTRETRY")
    private LocalDateTime lastRetry;
    //@OneToOne(cascade = CascadeType.ALL)
    //private TraktAccessToken traktAccessToken;
    //@OneToMany
    //List<ContentProviderDB> contentProvider;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String name) {
        this.username = name;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    public Integer getLoginRetry() {
        if(loginRetry == null)loginRetry=0;
        return loginRetry;
    }

    public void setLoginRetry(Integer loginRetry) {
        this.loginRetry = loginRetry;
    }
    public boolean canRetry(){
        if(getLoginRetry() < 3){
            return true;
        }
        if(lastRetry == null){
            return true;
        }
        if(getLoginRetry() <20){
            return LocalDateTime.now().minus(1L,ChronoUnit.MINUTES).isAfter(lastRetry);
        }
        return  LocalDateTime.now().minus(1L * getLoginRetry(), ChronoUnit.MINUTES).isAfter(lastRetry);
    }

    public void countFailedRetry() {
        if(loginRetry == null)loginRetry=0;
        loginRetry = loginRetry +1;
        lastRetry = LocalDateTime.now();
    }

    /*
    public TraktAccessToken getTraktAccessToken() {
        return traktAccessToken;
    }

    public void setTraktAccessToken(TraktAccessToken traktAccessToken) {
        this.traktAccessToken = traktAccessToken;
    }

    public List<ContentProviderDB> getContentProvider() {
        return contentProvider;
    }

    public void setContentProvider(List<ContentProviderDB> contentProvider) {
        this.contentProvider = contentProvider;
    }

     */
}
