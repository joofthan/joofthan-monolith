package com.joofthan.apps.login.boundary;

import com.joofthan.apps.login.adapter.jwt.UserTokenGenerator;
import com.joofthan.apps.login.boundary.html.MainHtml;
import com.joofthan.apps.login.control.UserServiceImpl;
import com.joofthan.lib.Headers;
import com.joofthan.lib.template.Template;
import com.joofthan.lib.user.UserId;
import com.joofthan.lib.user.UserModel;

import jakarta.inject.Inject;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.Cookie;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.NewCookie;
import jakarta.ws.rs.core.Response;


@Path("/login")
@Produces({MediaType.APPLICATION_JSON,MediaType.TEXT_HTML})
public class LoginEndpoint {
    @Inject
    UserTokenGenerator userTokenGenerator;

    @Inject
    UserServiceImpl userService;

    @Inject
    Template template;

    @POST
    @Path("api/register")
    public void register(LoginUserRequest req){
        userService.register(req.username(), req.password());
    }

    @POST
    @Path("api/token")
    public Response login(LoginUserRequest req, @CookieParam("Baerer")Cookie cookie){
        TokenResponse tokenResponse = new TokenResponse(userService.login(req.username(), req.password()));
        NewCookie newCookie = new NewCookie(
                "Baerer",
                tokenResponse.access_token(),
                "/",
                null,
                null,
                userTokenGenerator.getTimeoutSeconds(),
                false,
                true
        );
        return Response.ok(tokenResponse, MediaType.APPLICATION_JSON)
                .cookie(newCookie).build();
    }

    @GET
    @Path("api/me/name")
    @Produces({MediaType.TEXT_PLAIN})
    public Response meName(@HeaderParam(Headers.HX_REQUEST) boolean hxRequest){
        return Response.ok(userService.getUser().getName()).build();
    }

    @GET
    @Path("api/me")
    public UserResponse me(@HeaderParam(Headers.HX_REQUEST) boolean hxRequest){
        UserModel user = userService.getUserOrNull();
        if(user == null){
            user=new UserModel(new UserId(null));
            user.setName(null);
        }
        UserResponse userResponse = new UserResponse(
                user.getUserId().id(),
                user.getName(),
                user.getName() != null
        );

        return userResponse;
    }

    @POST
    @Path("api/logout")
    public Response logout(@CookieParam("Baerer") Cookie cookie, @HeaderParam(Headers.HX_REQUEST) boolean hxRequest){
        NewCookie newCookie = convertToExpired(cookie);
        if(hxRequest) return Response.ok(template.render(MainHtml.LOGIN_BUTTON), MediaType.TEXT_HTML).cookie(newCookie).build();
        return Response.ok(new LoginEndpoint.UserResponse(null,null,false),MediaType.APPLICATION_JSON).cookie(newCookie).build();
    }


    public static NewCookie convertToExpired(Cookie cookie) {
        NewCookie newCookie = new NewCookie(
                "Baerer",
                null,
                "/",
                null,
                null,
                0,
                false,
                false
        );
        return newCookie;
    }

    public record UserResponse(
      Long id,
      String name,
      boolean loggedIn
    ){}

    public record LoginUserRequest(
            String username,
            String password
    ) {}

    public record TokenResponse(
            String access_token
    ) {}
}
