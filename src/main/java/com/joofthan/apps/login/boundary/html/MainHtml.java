package com.joofthan.apps.login.boundary.html;

import com.joofthan.lib.template.Template;
import com.joofthan.apps.login.boundary.LoginEndpoint;

import jakarta.inject.Inject;
import jakarta.ws.rs.CookieParam;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.Cookie;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.NewCookie;
import jakarta.ws.rs.core.Response;

import static com.joofthan.apps.login.boundary.LoginEndpoint.convertToExpired;

@Produces(MediaType.TEXT_HTML)
@Path("login")
public class MainHtml {
    public static String LOGIN_BUTTON = "<a href=\"/login/\">Login</a>";

    @Inject
    Template template;

    @Inject
    LoginEndpoint loginEndpoint;

    @GET
    public String get(){
        //language="html"
        return """
                <!DOCTYPE html>
                <html >
                <head>
                    <title>Login</title>
                </head>
                <body>
                <script type="module">
                    import {el, onClick, post} from "../shared/apputil/apputil.js";
                                
                    let backTo = document.referrer;
                    onClick("#login", login);
                    onClick("#register", ()=>{
                        let user = el("#user").value;
                        let pw = el("#pw").value;
                        post("/login/api/register", {'username':user,'password':pw}).then((res)=>{
                            alert("registered");
                        });
                    });
                    addEventListener("keydown", e=>{
                    if(e.code === "Enter"){
                        login();
                    }
                    });
                    
                    function login() {
                       let user = el("#user").value;
                       let pw = el("#pw").value;
                       post("/login/api/token", {'username':user,'password':pw}).then((res)=>{
                           location.href=backTo;
                       });
                    }
                </script>
                        Username: <input id="user" name="username"><br/>
                        Password: <input id="pw" type="password" name="pw"/><br/>
                        <button id="login" type="submit">Login</button>
                        <button id="register">Register</button>
                </body>
                </html>
                """;
    }

    @GET
    @Path("logout")
    @Produces({MediaType.TEXT_HTML})
    public Response get(@CookieParam("Baerer") Cookie cookie){
        NewCookie newCookie = convertToExpired(cookie);

        return Response.ok("""
                <!DOCTYPE html>
                <html><head>
                    <title>Logout</title></head><body>
                   \s
                    <br />
                    <br />
                    <a href="/">Go to home</a></div></body>
                </html>
                """).cookie(newCookie).build();
    }

    @GET
    @Path("html/me")
        public String me(){
            return template.render("""
                {#if user.loggedIn}
                    {user.name} <a href="#" hx-post="/login/api/logout" hx-target="#user">Logout</a>
                {#else}
                    {loginButton}
                {/if}
                """.replace("{loginButton}", LOGIN_BUTTON),"user", loginEndpoint.me(false));
        }

}
