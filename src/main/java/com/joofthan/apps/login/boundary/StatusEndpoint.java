package com.joofthan.apps.login.boundary;

import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;

@Path("/login/status")
public class StatusEndpoint {

    @Inject
    EntityManager em;

    @GET
    public Status get(){
        return new Status(
                em.createQuery("select count(u) from UserDB u", Long.class).getResultList().get(0)
        );
    }

    public record Status(
            Long allUsers
    ) {}
}
