package com.joofthan.apps.login.control;

import com.joofthan.apps.login.adapter.hasher.PasswordHasher;
import com.joofthan.apps.login.adapter.jwt.UserTokenGenerator;
import com.joofthan.apps.login.entity.UserDB;
import com.joofthan.lib.Log;
import com.joofthan.lib.user.UserId;
import com.joofthan.lib.user.UserModel;
import com.joofthan.lib.user.UserNotLoggedIn;
import com.joofthan.lib.user.UserService;
import jakarta.enterprise.context.RequestScoped;
import jakarta.inject.Inject;
import org.eclipse.microprofile.jwt.Claim;

@RequestScoped
public class UserServiceImpl implements UserService {

    @Inject
    UserTokenGenerator userTokenGenerator;
    @Inject
    UserRepository userRepository;
    @Inject
    PasswordHasher passwordHasher;
    @Inject
    AutoLoginManager autoLoginManager;

    @Inject
    @Claim("userId")
    Long userId;

    UserModel userModel;

    public void register(String user, String password) {
        UserDB userModel = new UserDB();
        userModel.setUsername(user);
        userModel.setPasswordHash(passwordHasher.hash(password));
        userRepository.addNewUser(userModel);
    }

    public String login(String user, String password) {
        UserDB u = userRepository.findByUsername(user);
        if(!u.canRetry())throw new RuntimeException("Too many retries. Try again later.");
        if (!passwordHasher.isValid(password,u.getPasswordHash())) {
            u.countFailedRetry();
            userRepository.merge(u);
            throw new RuntimeException("Wrong Username or Password");
        }
        u.setLoginRetry(0);
        userRepository.merge(u);
        try{
            String generate = userTokenGenerator.generate(u.getId());
            return generate;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public UserModel getUser() {
        if(userModel == null){
            userModel = getUserOrNull();
            if (userModel == null) {
                throw new UserNotLoggedIn();
            }
        }
        return userModel;
    }

    public UserModel getUserOrNull() {
        if(userModel == null){
            userModel = loadUserModel();
            if(userModel == null && autoLoginManager.shouldDoAutologin()){
                userModel = doAutoLogin();
            }
        }
        return userModel;
    }

    private UserModel doAutoLogin() {
        try{
            register(autoLoginManager.getUsername(),autoLoginManager.getPassword());
            Log.info("Mock: User admin automatically registered");
        }catch (Exception e){
            System.out.println(e.getMessage());
            //Expected. Most of the time user is already registered
        }
        login(autoLoginManager.getUsername(),autoLoginManager.getPassword());
        Log.info("Mock: User "+autoLoginManager.getUsername()+" automatically logged in");
        autoLoginManager.markExecuted();
        return loadUserModel();
    }

    private UserModel loadUserModel() {
        if (userId == null) {
            return null;
        }
        UserDB user = userRepository.findById(userId);
        if(user == null){
            return null;
        }
        var userModel = new UserModel(new UserId(user.getId()));
        userModel.setName(user.getUsername());
        return userModel;
    }


    public boolean isLoggedIn() {
        return getUserOrNull() != null;
    }

}
