package com.joofthan.apps.login.control;

import com.joofthan.lib.GlobalConfig;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import java.io.Serializable;

@ApplicationScoped
class AutoLoginManager implements Serializable {

    @Inject
    GlobalConfig globalConfig;

    private boolean wasExecuted;

    public boolean shouldDoAutologin() {
        return globalConfig.isAutologinEnabled() && !wasExecuted;
    }

    public void markExecuted(){
        wasExecuted=true;
    }

    public String getUsername() {
        return "admin";
    }

    public String getPassword() {
        return "admin";
    }

    public boolean autologinEnabled() {
        return globalConfig.isAutologinEnabled();
    }
}
