package com.joofthan.apps.login.control;

import com.joofthan.apps.login.entity.UserDB;

import jakarta.enterprise.context.RequestScoped;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.transaction.Transactional;

import java.util.List;


@Transactional
@RequestScoped
class UserRepository {

    @Inject
    EntityManager em;

    public void addNewUser(UserDB userDB) {
        if(findByUsername(userDB.getUsername()) != null){
            throw new RuntimeException("UserExists");
        }
        notEmpty(userDB.getUsername());
        notEmpty(userDB.getPasswordHash());
        em.persist(userDB);
    }

    public void merge(UserDB userDB) {
        notEmpty(userDB.getUsername());
        notEmpty(userDB.getPasswordHash());
        em.merge(userDB);
    }

    private void notEmpty(String value){
        if(value == null || value.isBlank()){
            throw new IllegalArgumentException("Value is empty");
        }
    }

    public UserDB findByUsername(String user) {
        List<UserDB> userList = em.createQuery("select u from UserDB u where u.username like :name", UserDB.class)
                .setParameter("name", user)
                .getResultList();
        if(userList.size() == 0){
            return null;
        }
        else if(userList.size() > 1 ){
            throw new RuntimeException("Mehr als 1 user mit dem name: "+user);
        }
        return userList.get(0);
    }

    public UserDB findById(Long userId) {
        return em.find(UserDB.class, userId);
    }
}
