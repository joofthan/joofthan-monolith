package com.joofthan.apps.login.adapter.jwt;

import com.joofthan.lib.Log;
import io.smallrye.jwt.build.Jwt;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import jakarta.enterprise.context.RequestScoped;
import java.io.Serializable;
import java.util.Arrays;
import java.util.HashSet;

@RequestScoped
public class UserTokenGenerator implements Serializable {

    @ConfigProperty(name = "joofthan.session.timeout.seconds")
    int timeoutSeconds;

    public String generate(Long userId){
        Log.info("Using only Test User Token");
        String token =
                Jwt.issuer("https://joofthan.com/login/issuer")
                        .upn(userId.toString())
                        .expiresIn(timeoutSeconds)
                        .groups(new HashSet<>(Arrays.asList("User")))
                        .claim("userId",userId)
                        //.claim(Claims.birthdate.name(), "2001-07-13")
                        .sign();
        return token;
    }

    public int getTimeoutSeconds() {
        return timeoutSeconds;
    }
}
