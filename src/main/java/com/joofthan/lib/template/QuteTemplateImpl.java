package com.joofthan.lib.template;

import io.quarkus.qute.Qute;

import jakarta.enterprise.context.RequestScoped;
import java.util.Map;

@RequestScoped
public class QuteTemplateImpl implements Template {
    @Override
    public String render(String template){
        return Qute.fmt(template).render();
    }
    @Override
    public String render(String template, String key, Object obj){
        return Qute.fmt(template).data(key,obj).render();
    }
    @Override
    public String render(String template, Map<String, Object> map){
        return Qute.fmt(template).dataMap(map).render();
    }
}
