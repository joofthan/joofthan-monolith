package com.joofthan.lib;

import jakarta.enterprise.context.ApplicationScoped;
import lombok.Getter;
import org.eclipse.microprofile.config.inject.ConfigProperty;

@Getter
@ApplicationScoped
public class GlobalConfig {

    @ConfigProperty(name="joofthan.mock.autologin")
    boolean autologinEnabled;
    @ConfigProperty(name="joofthan.admin")
    String admin;
    @ConfigProperty(name="joofthan.devmode")
    boolean isDevMode;
    @ConfigProperty(name = "joofthan.spoonacular.key")
    String spoonacularKey;

}