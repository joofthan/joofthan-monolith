package com.joofthan.lib.user;

import java.io.Serializable;

public interface UserService extends Serializable {
    UserModel getUser();
    boolean isLoggedIn();
    void register(String user, String password);
    String login(String user, String password);
}