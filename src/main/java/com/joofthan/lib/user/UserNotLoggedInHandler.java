package com.joofthan.lib.user;

import com.joofthan.apps.multithek.backend.home.entity.Link;
import com.joofthan.apps.login.boundary.LoginEndpoint;

import jakarta.annotation.Priority;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;

@Priority(10000)
@Provider
public class UserNotLoggedInHandler implements ExceptionMapper<UserNotLoggedIn> {
    @Override
    public Response toResponse(UserNotLoggedIn exception) {
        //language="html"
        return Response.status(200).header("HX-Redirect", Link.to(LoginEndpoint.class)).build();
    }
}

