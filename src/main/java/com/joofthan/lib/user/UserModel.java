package com.joofthan.lib.user;

public class UserModel {
    private UserId userId;
    private String name;
    private String passwordHash;

    public UserModel(UserId userId){
        if(userId == null){
            throw new IllegalArgumentException("UserId can not be null");
        }
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    public UserId getUserId() {
        return userId;
    }
}
