package com.joofthan.lib.user;

public record UserId(Long id) {}
