package com.joofthan;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.inject.Produces;
import jakarta.enterprise.inject.spi.InjectionPoint;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceUnit;

@ApplicationScoped
public class EntityManagerProducer {

    @PersistenceUnit(unitName = "test")
    EntityManager emCache;
    @PersistenceUnit(unitName = "login")
    EntityManager emLogin;
    @PersistenceUnit(unitName = "mealplanner")
    EntityManager emMealplanner;
    @PersistenceUnit(unitName = "multithek")
    EntityManager emMultithek;
    @PersistenceUnit(unitName = "myhome")
    EntityManager emMyHome;

    @Produces
    public EntityManager create(InjectionPoint ip){
        if(isPackage(ip,"apps.login")){
            return emLogin;
        }
        if (isPackage(ip, "apps.multithek")){
            return emCache;//TODO: real db
        }
        if(isPackage(ip,"apps.myhome")){
            return emMyHome;
        }
        if(isPackage(ip,"apps.mealplanner.backend")) {
                return emMealplanner;
        }
        if(isPackage(ip,"apps.mealplanner.cookingapi")){
                return emCache;//TODO: real db
        }
        if(isPackage(ip,"apps.logger")){
            return emCache;//TODO: real db
        }

        throw new RuntimeException("No EntityManager found for: \""+ip.getMember().getDeclaringClass().getTypeName()+ "\" in "+EntityManagerProducer.class.getName());
    }

    private static boolean isPackage(InjectionPoint ip, String name) {
        return ip.getMember().getDeclaringClass().getTypeName().startsWith("com.joofthan."+name);
    }

}
