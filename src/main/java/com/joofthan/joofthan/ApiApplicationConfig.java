package com.joofthan.joofthan;

import org.eclipse.microprofile.openapi.annotations.Components;
import org.eclipse.microprofile.openapi.annotations.OpenAPIDefinition;
import org.eclipse.microprofile.openapi.annotations.enums.SecuritySchemeType;
import org.eclipse.microprofile.openapi.annotations.info.Info;
import org.eclipse.microprofile.openapi.annotations.security.SecurityRequirement;
import org.eclipse.microprofile.openapi.annotations.security.SecurityScheme;

import jakarta.ws.rs.ApplicationPath;
import jakarta.ws.rs.core.Application;


@OpenAPIDefinition(
        info = @Info(
        title = "My API",
        version = "1.0.0"
),
        components = @Components(
        securitySchemes = {
                @SecurityScheme(
                        securitySchemeName = "bearerAuth",
                        type = SecuritySchemeType.HTTP,
                        scheme = "bearer",
                        bearerFormat = "JWT"
                )
        }
),
        security = {
@SecurityRequirement(name = "bearerAuth")
    }
            )
@ApplicationPath("/")
public class ApiApplicationConfig extends Application {


}
