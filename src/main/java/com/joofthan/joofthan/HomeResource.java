package com.joofthan.joofthan;

import org.eclipse.microprofile.config.inject.ConfigProperty;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;

@Path("/api/devlink")
public class HomeResource {

    @ConfigProperty(name = "joofthan.devmode")
    boolean devmode;

    @GET
    public String getU(){
        if(devmode) return "<a href=\"/dev\">Dev</a></br></br>";
        else return "";
    }

}
