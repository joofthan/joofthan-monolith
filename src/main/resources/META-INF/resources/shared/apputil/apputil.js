let appUtilDebugging = true;
function log(msg){
    if(appUtilDebugging){
        console.log(msg);
    }
}
// customized: true
console.log(`AppUtil (0.4.3)
 - Static Helpers (load, onClick, el, inputElement, buttonElement)
 - AppElement
 - ShadowElement
 - AppNavigatorBase
 - NotEmpty
 - RestClient
 - AppState`);

/******************************************************************
 *           Static Helpers
 ******************************************************************/
let wasNotified = false;
function assert(cond, msg, content){
    if(cond !== true){
        console.error('Assertion Failed: '+msg, content);
        if(!wasNotified){
            wasNotified = true;
            alert(msg);
        }
    }
}

/**
 *
 * @param path
 * @returns {LoadResult}
 */
export function load(path){
    class LoadResult{
        path;
        constructor(path) {
            this.path = path;
        }

        /**
         *
         * @param selector
         * @returns {Promise<void>}
         */
        async into(selector){
            document.querySelector(selector).innerHTML=await (await fetch(this.path)).text();
        }

        /**
         *
         * @param {function(NotEmpty):void} callback
         * @returns {Promise<void>}
         */
        async then(callback){
            callback(await new RestClient().get(path));
        }
    }
    return new LoadResult(path);
}

/**
 *
 * @param path
 * @returns {Promise<NotEmpty>}
 */
export async function post(path, obj){
    return await new RestClient().post(path,obj);
}

/**
 * @returns {HTMLElement}
 */
export function el(select){
    let element = document.querySelector(select);
    assert(element != null, "Element "+select+" not found", select);
    return element;
}

/**
 * @returns {HTMLInputElement}
 */
export function inputElement(select){
    return el(select);
}
/**
 * @returns {HTMLButtonElement}
 */
export function buttonElement(select){
    return el(select);
}

/**
 *
 * @param select
 * @param {function(Event):void} callback
 */
export function onClick(select, callback){
    addEvent('click',select,callback);
}

/**
 *
 * @param select
 * @param {function(Event):void} callback
 */
export function onChange(select, callback){
    addEvent('change',select,callback);
}

function addEvent(name,select, callback){
    if(select instanceof HTMLElement){
        select.addEventListener(name, callback);
    } else{
        let nodeListOf = document.querySelectorAll(select);
        nodeListOf.forEach(e=>{
        e.addEventListener(name, callback);
        });
        if(nodeListOf.length === 0)el(select);
    }
}

/**
 *   attributes: {'name':'value'}
 *   contentText: 'hello'
 *   contentText: {HTMLElement} //create('p')
 *
 * @param tag
 * @param attrKeyValueObj
 * @param contentStringOrElementArray
 * @returns {HTMLElement}
 */
export function create(tag, attrObj = {}, content = null){
    let elem = document.createElement(tag);
    for (const attr in attrObj) {
        elem.setAttribute(attr, attrObj[attr]);
    }
    if(content === null){
        //nothing
    }else if(typeof content === 'string') {
        elem.innerText = content;
    }else{
        if(Array.isArray(content)){
            for (let i = 0; i<content.length; i++) {
                elem.append(content[i]);
            }
        }else {
            elem.append(content);
        }
    }

    return elem;
}

/**
 *
 * @param name
 * @param value
 * @param event
 * @returns {HTMLElement}
 */

export function createInput(name, value, event) {
    let input8 = create('input',  {'placeholder': name, 'type': 'text', 'value':value});
    input8.addEventListener('keyup', (e)=> event(e.target.value));
    return input8;
}

/******************************************************************
 *           AppElement
 ******************************************************************/
export class AppElement extends HTMLElement{
    constructor() {
        super();
    }
    connectedCallback(){
        log('AppElement.connectedCallback was called');
    }
    static get observedAttributes() {
        throw new Error('observedAttributes is not overridden');
        return [];
    }
    render(html){
        log('AppElement.render');
        this.innerHTML=html;
    }
    attr(key){
        let con = this.getAttribute(key);

        return con;
    }
    createInput(name, value, event){
        return create(name,value,event);
    }

    create(tag, attrObj = {}, content = null){
        return create(tag,attrObj,content);
    }
    /**
     * @returns {HTMLElement}
     */
    el(select){
        return this.querySelector(select);
    }

    /**
     * @returns {HTMLInputElement}
     */
    input(select){
        return this.el(select);
    }
    /**
     * @returns {HTMLButtonElement}
     */
    button(select){
        return this.el(select);
    }
}
/******************************************************************
 *           ShadowElement
 ******************************************************************/
export class ShadowElement extends AppElement{
    shadow = null;
    constructor() {
        super();
        this.shadow = this.attachShadow({mode: 'open'});
    }
    connectedCallback(){
        log('ShadowElement.connectedCallback was called');
    }
    render(html){
        log('ShadowElement.render');
        this.shadow.innerHTML=html;
    }
    attr(key){
        return this.getAttribute(key);
    }
    /**
     * @returns {HTMLElement}
     */
    el(select){
        return this.shadow.querySelector(select);
    }
}
/******************************************************************
 *           AppNavigatorBase
 ******************************************************************/
export class AppNavigatorBase{
    page = "/";
    //_pageStore = writable("");
    listeners = [];
    constructor(){
        //this._pageStore.subscribe((val)=>this.page = val);
    }
    home(){
        this.navigate("/");
    }
    isHome(){
        return this.isPage("/");
    }
    getParam(name){
        try{
            let paramPart = this.page.split("?")[1];
            let na = "&"+paramPart+"&";
            let valueStart = na.split("&"+name+"=")[1];
            let value = valueStart.split("&")[0];
            return value;
        }catch(e){
            throw new Error("Param "+name+" not found. On: "+this.page+"."+e);
        }
    }

    navigate(url){
        log("navigate to: "+ url);
        this.page = url;
        this.listeners.forEach((l)=>l());
        //this._pageStore.set(url);
        //location.href = url;
    }
    isPage(url){
        return (this.page.split("?")[0]) === url;
    }

    onChange(fun){
        //this._pageStore.subscribe(fun);
        this.listeners.push(fun);
    }

    isReady(){
        return this.page != null;
    }
}
/******************************************************************
 *           NotEmpty
 ******************************************************************/

export class NotEmpty{
    constructor(content) {
        assert(content !== undefined, "NotNull is Empty", content);
        this.content = content;
    }

    value(){
        return this.content;
    }
    jsonText(){
        return JSON.stringify(this.content);
    }

    /**
     *
     * @param key
     * @param allowEmpty
     * @returns {NotEmpty}
     */
    get(key, allowEmpty = false){
        return this.load(key, allowEmpty);
    }

    load(key, allowEmpty = false){
        let value = this.content[key];
        if(!allowEmpty){
            assert(value !== undefined, "Property \""+key + "\" is empty on "+this.content, this.content);
        }
        return NotEmpty.of(value);
    }

    static of(content) {
        return new NotEmpty(content);
    }
}
/******************************************************************
 *           RestClient
 ******************************************************************/
export class RestClient {
    url = '';
    token = null;
    constructor(url){
        if(url){
            this.url = url;
        }
    }
    async getHeaders(){
        let headers = new Headers();
        if(this.token != null){
            headers.set('Authorization', 'Bearer ' + this.token);
        }
        headers.set('Content-Type', 'application/json');
        return headers;
    }
    static async create(url){
        let rc = new RestClient(url);
        return rc;
    }
    async post(uri, obj){
        return NotEmpty.of(
            await fetch(this.url + uri, {
                method: 'POST',
                credentials: "include",
                headers: await this.getHeaders(),
                body: JSON.stringify(obj)
            }).then((f)=>RestClient.handleResponse(f, this.url + uri))
                .catch(e=>RestClient.handleError(e, this.url + uri, "post", obj))
        );
    }

    /**
     *
     * @param uri
     * @returns {Promise<NotEmpty>}
     */
    async get(uri){
        return NotEmpty.of(await fetch(this.url + uri, {method:"GET", credentials: "include", headers: await this.getHeaders()})
            .then((f)=>RestClient.handleResponse(f, this.url + uri))
            .catch(e=>RestClient.handleError(e, this.url + uri, "get")));
    }
    async delete(uri){
        await fetch(this.url + uri, {
            method: 'DELETE',
            credentials: "include",
            headers: await this.getHeaders()
        })
            .then((f)=>RestClient.handleResponse(f, this.url + uri))
            .catch(e=>RestClient.handleError(e, this.url + uri, "delete"));
    }
    async put(uri, obj){
        return await fetch(this.url + uri, {
            method: 'PUT',
            credentials: "include",
            headers: await this.getHeaders(),
            body: JSON.stringify(obj)
        })
            .then((f)=> {
                if(f.status === 200){
                    return f.json();
                }else{
                    RestClient.handleResponse(f,this.url + uri)
                }
            })
            .catch(e=>RestClient.handleError(e, this.url + uri, "put", obj));
    }
    static async handleResponse(res, url) {
        function prop(key, value){
            return key + ": " + value + "\n";
        }
        function text(text){
            return text+"\n\n";
        }
        let status = res.status;
        if(res.ok){
            return res.json();
        } if(status === 404){
            console.log(url);
            alert(`Request: ${url.toString()}
                   Response: 404 Not Found`);
        } else {
            let message = text("Status Text: "+res.statusText)
                +prop("http statusCode", status)
                +prop("url", url);
            console.log("Look at XHR response for details");
            console.log("Unexpected Response: AppUtil.js in line:")
            console.log(res);
            console.log(message);
            alert(
                text("Unexpected Response: Details in console.log")
                +message
            );
            return Promise.reject(res.text());
        }
    }
    static handleError(e, url, methodName, data = null){
        function prop(key, value){
            return key + ": " + value + "\n";
        }

        function text(text){
            return text+"\n\n";
        }

        let message = text("Error: "+e.error)
            +prop("message", e.message)
            +prop("methodName", methodName)
            +prop("url", url)
            +prop("data", JSON.stringify(data))
            +prop("trace", e.trace);

        console.log("Error: AppUtil.js in line:")
        console.log(e);
        console.log(e.trace);
        console.log(message);

        alert(
            message
            + text("\nError: Details in console.log")
        );

        return Promise.reject(e.message);
    }
    setToken(token) {
        this.token = token;
    }
    hasToken() {
        return this.token != null;
    }
}

export class AppState {
    _myState = {};
    _listener = [];
    get(componentName){
        return this._myState[componentName];
    }
    set(componentName, content){
        this._myState[componentName] = content;
        this._listener.forEach(l => l());
        log('getting '+componentName+' from:');
        log(this._myState);
    }
    onChange(componentName, method){
        this._listener.push(method);
    }
}

export const state = new AppState();

