import {load, RestClient} from "../../shared/apputil/apputil.js";


export const api = new RestClient("/myhome/api/");

export function checkLoggedIn(){
    fetch("/login/api/me").then(async res=>{
        if(res.ok){
            let jsonResponse = await res.json();
            if(jsonResponse.loggedIn){

            }else{
                location.href="/login/";
            }
        }else{
            alert("Error checking User");
            location.href="/login/";
        }
    });
}
