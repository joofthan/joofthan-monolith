package com.joofthan.annotplanner;

public @interface Microservice {
    Interactor[] interactors();
    String name();
    String packageName();
}
