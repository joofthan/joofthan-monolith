package com.joofthan.annotplanner;

public @interface Interactor {
    String name() default "";
    Type type();
    String role() default "";
    Command[] commands() default {};
    Query[] queries() default {};
    Listener[] listens() default {};

    Progress planningState();
}
