package com.joofthan.annotplanner;

public enum Type {
    USER,
    INTERNAL_API,
    EXTERNAL_API,
    FILES_IO,
}
