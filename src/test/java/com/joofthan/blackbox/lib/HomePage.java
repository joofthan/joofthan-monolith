package com.joofthan.blackbox.lib;

public class HomePage extends TestLibrary.BasePage {

    public void login(String user, String pw){
        type("input #username", user);
        type("input #password", pw);
        click("button #login");
    }

    public boolean isLoggedIn(){
        return isVisible("#logout");
    }
}
