package com.joofthan.blackbox.lib;

import com.microsoft.playwright.Browser;
import com.microsoft.playwright.Locator;
import com.microsoft.playwright.Page;
import com.microsoft.playwright.Playwright;
import org.eclipse.microprofile.rest.client.RestClientBuilder;

import java.net.MalformedURLException;
import java.net.URL;

public class TestLibrary {
    //URL
        private static String url;
        public static String getBaseUrl() {
            if(url == null){
                url = getIfNull(null, System.getProperty("base.url"));
                url = getIfNull(url, System.getenv("BASE_URL"));
                url = getIfNull(url, "http://localhost:8213");
                System.out.println("TestLibrary: Using URL "+url);
            }
            return url;
    }
    private static String getIfNull(String current, String newValue){
            if(current == null) return newValue;
            else return current;
    }
    //CLIENT
    public static <T> T create(Class<T> clazz){
        try {
            return RestClientBuilder.newBuilder().baseUrl(new URL(getBaseUrl())).build(clazz);
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }
    //UI
    private static Page page;
    public static synchronized Page getPlaywrightPage(){
        if(page == null){
            System.out.println("Playwright: started");
            Playwright p = Playwright.create();
            Browser browser = p.firefox().launch();
            page = browser.newPage();
            Runtime.getRuntime().addShutdownHook(new Thread(()->{
                page.close();
                browser.close();
                p.close();
                System.out.println("Playwright: closed");
            }));
        }
        return page;
    }
    //Classes
    public static class BasePage {
        protected Page p = getPlaywrightPage();
        protected Locator find(String selector) {
            return p.locator(selector);
        }
        protected void click(String selector) {
            p.click(selector);
        }
        protected void type(String selector, String text) {
            p.type(selector, text);
        }
        protected boolean isVisible(String selector){
            return p.isVisible(selector);
        }
        public void navigate(String path) {
            p.navigate(getBaseUrl() + path);
        }
    }
}