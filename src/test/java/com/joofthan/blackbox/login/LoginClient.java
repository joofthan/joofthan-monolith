package com.joofthan.blackbox.login;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;

@Path("/login")
public interface LoginClient {
    @GET
    String get();
}
