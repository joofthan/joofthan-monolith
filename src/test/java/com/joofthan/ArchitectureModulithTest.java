package com.joofthan;

import com.joofthan.annotplanner.*;
import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import com.tngtech.archunit.core.importer.ImportOption;
import com.tngtech.archunit.library.dependencies.SlicesRuleDefinition;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.classes;


@Modulith({
        @Microservice(
                name ="Multithek",
                packageName="com.joofthan.multithek.multithek",
                interactors = {
                        @Interactor(type= Type.USER,
                                planningState = Progress.UNFINISHED,
                                commands = {
                                        @Command("List All content on Home Page"),
                                        @Command(
                                                value = "AddContentProvider",
                                                parameters = Plan.URL
                                        )
                                }
                        ),
                        @Interactor(type=Type.INTERNAL_API,
                                name = "Content Providers",
                                planningState = Progress.TODO
                        )

                }
        ),
        @Microservice(
                name = "ContentProvider Example",
                packageName = "com.joofthan.multithek.providers.example",
                interactors = {
                        @Interactor(type = Type.EXTERNAL_API,
                                name = "Content Api",
                                planningState = Progress.TODO,
                                queries = @Query(
                                        "Load Content from netflix.com"
                                )
                        )
                }
        ),
        @Microservice(
                name = "ProgressService",
                packageName = "com.joofhtan.multithek.trakt",
                interactors = {
                        @Interactor(type=Type.EXTERNAL_API,
                                name = "Trakt.TV",
                                planningState = Progress.UNFINISHED,
                                commands = {
                                        @Command(
                                                value = "SAVE PROGRESS",
                                                parameters = Plan.IMDB_ID
                                        )
                                },
                                queries = {
                                        @Query(
                                                value = "Get progress of content",
                                                parameters = Plan.IMDB_ID
                                        )
                                }
                        )
                }
        )
})

public class ArchitectureModulithTest {

    @Test
    void allApplicationsAreIndependent(){
        JavaClasses classes = new ClassFileImporter().importPackages("com.joofthan");
        SlicesRuleDefinition.slices().matching("com.joofthan.apps.(*)..")
                .should().notDependOnEachOther()
                .check(classes);
    }


    @Disabled
    @Test
    void jakartaEEApisOnly(){
        JavaClasses classes = new ClassFileImporter()
                .withImportOption(
                        new ImportOption.DoNotIncludeTests())
                .importPackages("com.joofthan");
        classes().that()
                .resideOutsideOfPackages("com.joofthan.lib.(**)", "com.joofthan.apps.(*).adapter.(**)", "com.joofthan.joofthan.(*).adapter.(**)").should()
                .onlyDependOnClassesThat()
                .resideInAnyPackage(
                        "com.joofthan.(**)",

                        //Generated
                        "org.openapi.quarkus.spoonacular_json.model",

                        //Java Apis
                        "java.(**)",
                        "javax.(**)",
                        "jakarta.(**)",
                        "org.eclipse.microprofile.(**)",

                        //Exceptions for Prototyping
                        "io.quarkus.hibernate.orm.panache",
                        "org.hibernate.annotations"
                )
                .check(classes);
    }

}
