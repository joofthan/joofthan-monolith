package com.joofthan;

import com.joofthan.lib.user.UserId;
import com.joofthan.lib.user.UserModel;
import com.joofthan.lib.user.UserService;
import io.quarkus.test.junit.mockito.InjectMock;
import org.junit.jupiter.api.BeforeEach;

import static org.mockito.Mockito.when;

public class BaseTest {


    @InjectMock
    public UserService userService;
    public UserModel user;

    @BeforeEach
    void initAdmin() {
        when(userService.isLoggedIn()).thenReturn(true);
        when(userService.getUser()).thenReturn(user(1,"admin"));
    }

    public static UserModel user(int id, String name) {
        var user = new UserModel(new UserId((long) id));
        user.setName(name);
        return user;
    }

    public void switchUserAdmin(){
        when(userService.getUser()).thenReturn(user(1,"admin"));
    }

    public void switchToPeter(){
        when(userService.getUser()).thenReturn(user(2,"peter"));
    }

    public void switchToMax(){
        when(userService.getUser()).thenReturn(user(3,"max"));
    }
}
