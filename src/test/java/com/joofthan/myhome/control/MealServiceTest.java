package com.joofthan.myhome.control;

import com.joofthan.BaseTest;
import com.joofthan.apps.myhome.meal.control.MealService;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;

import jakarta.inject.Inject;

import static org.junit.jupiter.api.Assertions.*;

@QuarkusTest
class MealServiceTest extends BaseTest {

    @Inject
    MealService mealService;

    @Test
    void should_return_meal_when_db_is_empty() {
        assertTrue(mealService.getToday().size()>=1);
    }
}