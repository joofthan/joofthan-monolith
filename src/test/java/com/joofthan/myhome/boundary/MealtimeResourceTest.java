package com.joofthan.myhome.boundary;

import com.joofthan.BaseTest;
import com.joofthan.apps.myhome.meal.boundary.MealtimeEndpoint;
import io.quarkus.test.junit.QuarkusTest;

import jakarta.inject.Inject;

@QuarkusTest
class MealtimeResourceTest extends BaseTest {

    @Inject
    MealtimeEndpoint mealtimeEndpoint;

    /*
    @Test
    void should_load_mealtimes_when_db_empty(){
        assertNotEquals(0, mealtimeResource.getMealTimes().size());
    }

    @Test
    void should_have_default_values_when_empty_meal_is_loaded(){
        var result = mealtimeResource.getMealTimes().get(0);
        assertNotNull(result.getUserId());
        assertNotNull(result.getTime());
    }

    @Test
    void should_have_three_mealtimes_a_day_when_empty_mealtimes_are_loaded(){
        assertTrue(mealtimeResource.getMealTimes().size()>=3);
    }

    @Test
    void should_have_id_when_empty_mealtime_is_loaded(){
        assertNotNull(mealtimeResource.getMealTimes().get(0).getId());
    }

    @Test
    void should_save_mealtime_when_post(){
        var t = mealtimeResource.getMealTimes().get(0);
        t.setTime(LocalTime.of(12,2,2));
        mealtimeResource.save(t);
        assertEquals(LocalTime.of(12,2,2), mealtimeResource.getMealTimes().get(0).getTime());
    }

     */
}