package com.joofthan.myhome.boundary;

import com.joofthan.BaseTest;
import com.joofthan.apps.myhome.meal.boundary.MealEndpoint;
import io.quarkus.test.junit.QuarkusTest;
import jakarta.inject.Inject;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


@QuarkusTest
class MealEndpointTest extends BaseTest {

    @Inject
    MealEndpoint mealEndpoint;

    @Test
    void should_load_meal_when_db_empty(){
        assertNotEquals(0, mealEndpoint.get().size());
    }

    @Test
    void should_have_default_values_when_empty_meal_is_loaded(){
        var days = mealEndpoint.get().get(0);
        assertFalse(days.meals().get(0).shopped());
        assertFalse(days.meals().get(0).eaten());
    }

    @Test
    void should_have_at_least_one_meals_a_day_when_empty_meals_are_loaded(){
        assertTrue( mealEndpoint.get().size() >=1 );
    }
}
