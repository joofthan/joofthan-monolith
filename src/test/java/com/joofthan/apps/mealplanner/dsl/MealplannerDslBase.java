package com.joofthan.apps.mealplanner.dsl;

import org.junit.jupiter.api.BeforeEach;

/**
 * Class providing access to Domain Specific Language for tests.
 */
public class MealplannerDslBase {

    protected MealPlannerAppTestContainer app;
    private Given given;
    private When when;
    private Then then;

    @BeforeEach
    void initDslObject(){
        app = new MealPlannerAppTestContainer();
        given = new Given(app);
        when = new When(app);
        then = new Then(app);
    }


    public Given given() {
        return given;
    }

    public When when() {
        return when;
    }

    public Then then(){
        return then;
    }
}
