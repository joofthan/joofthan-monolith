package com.joofthan.apps.mealplanner.dsl;

import com.joofthan.apps.mealplanner.backend.MealPlannerEndpoint;
import jakarta.json.bind.JsonbBuilder;
import jakarta.ws.rs.core.Response;

/**
 * Class actions to test. Split in multiple if too big.
 */
public class When {


    private final MealPlannerEndpoint mealPlannerEndpoint;
    private final MealPlannerAppTestContainer app;

    public When(MealPlannerAppTestContainer app) {
        this.app = app;
        this.mealPlannerEndpoint = app.mealPlannerEndpoint;
    }

    public void user_registeres_with_email(String email) {
        //registerFormular.register(email, "Peter", "Pan", "2000-01-01");
    }

    public void user_searches_recipe(String query) {
        storeResponseJson(mealPlannerEndpoint.searchRecipes(query));
    }

    private <T> void storeResponseJson(Response response) {
        var o = JsonbBuilder.create();
        T entity = (T) response.getEntity();
        app.result = o.toJson(entity);
    }

    public void user_gets_bolognese_details_by_id(String id) {
        storeResponseJson(mealPlannerEndpoint.getRecipeById(id));
    }
}
