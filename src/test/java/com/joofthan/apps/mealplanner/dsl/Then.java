package com.joofthan.apps.mealplanner.dsl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Class for assertions. Split if to big.
 */
public class Then {

    private final MealPlannerAppTestContainer app;

    public Then(MealPlannerAppTestContainer app) {
        this.app = app;
    }

    public Then smtp_server_was_called_with_email(String email) {
        //verify(smtpServer).sendEmailWithHtml(eq(email),anyString(),anyString());
        return this;
    }

    public Then and() {
        return this;
    }

    public Then users_on_waitinglist(int numberOfUsers) {
        //assertEquals(dataRoot.getCandidates().size(), numberOfUsers);
        return this;
    }
    public Then users_on_unconfirmed_list(int numberOfUsers) {
        //assertEquals(dataRoot.getUnconfirmed().size(), numberOfUsers);
        return this;
    }

    public void responseIs(String expected) {
        assertEquals(expected,app.result + "\n");
    }

    public void response_is_spoonacular_bolognese() {
        responseIs("""
                [{"id":"spoonacular-1","image":"https://spoonacular.com/recipeImages/634900-556x370.jpg","title":"Bolognese"}]
                """);
    }

    public void responseContains(String toContain) {
        assertTrue(app.result.contains(toContain));
    }
}
