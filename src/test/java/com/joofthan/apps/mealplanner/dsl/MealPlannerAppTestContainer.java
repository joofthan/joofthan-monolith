package com.joofthan.apps.mealplanner.dsl;

import com.joofthan.apps.mealplanner.backend.MealPlannerEndpoint;
import com.joofthan.apps.mealplanner.backend.recipes.v1.AppRecipesApiEndpoint;
import com.joofthan.apps.mealplanner.backend.shoplist.CategoryEnricher;
import com.joofthan.apps.mealplanner.backend.shoplist.ShoppingItemRepository;
import com.joofthan.apps.mealplanner.backend.shoplist.ShoppingItemsEndpoint;
import com.joofthan.apps.mealplanner.backend.shoplist.ShoppingList;
import com.joofthan.apps.mealplanner.cookingapi.backend.boundary.v2023_07_01.CookingApiService;
import com.joofthan.apps.mealplanner.cookingapi.backend.control.RecipeDBRepository;
import com.joofthan.apps.mealplanner.cookingapi.backend.control.RecipeDBService;
import com.joofthan.apps.mealplanner.cookingapi.sources.spoonakular.boundary.SpoonacularService;
import com.joofthan.apps.mealplanner.cookingapi.sources.spoonakular.control.SpoonacularClient;
import com.joofthan.lib.user.UserService;

import static org.mockito.Mockito.mock;

/**
 * Domain specific language for describing bdd scenarios.
 * Contains all App components for running in memory bdd unit tests. Alternative to CDI Container for faster execution and testing logic only.
 */
public class MealPlannerAppTestContainer {

    //DB MOCKS
    RecipeDBRepository recipeDBRepository = mock(RecipeDBRepository.class);//RecipeDBIntegrationTest TODO
    ShoppingItemRepository shoppingItemRepositoryMock = mock(ShoppingItemRepository.class);//ShoppingItemRepositoryIntegrationTest TODO

    //API MOCKs
    UserService userServiceMock = mock(UserService.class); //No Test should work :D
    SpoonacularClient spoonacularClientMock = mock(SpoonacularClient.class); //SpoonacularClientIntegrationTest TODO

    //APP
    MealPlannerEndpoint mealPlannerEndpoint;

    //RESULT
    public String result;

    public MealPlannerAppTestContainer(){
        mealPlannerEndpoint = new MealPlannerEndpoint(
                new ShoppingItemsEndpoint(
                        new ShoppingList(
                                shoppingItemRepositoryMock,
                                new CategoryEnricher()
                        )
                ),
                new AppRecipesApiEndpoint(
                        new CookingApiService(
                                new SpoonacularService(spoonacularClientMock),
                                new RecipeDBService(recipeDBRepository),
                                null
                        ),
                        userServiceMock
                ),
                new SpoonacularService(spoonacularClientMock)
        );
    }
}
