package com.joofthan.apps.mealplanner.dsl;

import com.joofthan.apps.mealplanner.cookingapi.Parser;
import com.joofthan.apps.mealplanner.cookingapi.backend.ExampleData;
import com.joofthan.apps.mealplanner.cookingapi.backend.control.RecipeDBRepository;
import com.joofthan.apps.mealplanner.cookingapi.sources.spoonakular.control.SpoonacularClient;
import com.joofthan.apps.mealplanner.cookingapi.sources.spoonakular.entity.SpoonacularRecipeSearchResult;
import com.joofthan.apps.mealplanner.cookingapi.sources.spoonakular.entity.SpoonacularRecipeSearchResultList;
import com.joofthan.lib.user.UserId;
import com.joofthan.lib.user.UserModel;
import com.joofthan.lib.user.UserService;

import java.util.List;

import static org.mockito.Mockito.when;

/**
 * Class for test setup. Split in multiple if too big.
 */
public class Given {
    private final MealPlannerAppTestContainer app;
    private final SpoonacularClient spoonacularClientMock;
    private final UserService userServiceMock;
    private final RecipeDBRepository recipeDBRepository;

    public Given(MealPlannerAppTestContainer app) {
        this.app = app;
        this.spoonacularClientMock = app.spoonacularClientMock;
        this.userServiceMock = app.userServiceMock;
        this.recipeDBRepository = app.recipeDBRepository;
    }

    public static UserModel user(int id, String name) {
        var user = new UserModel(new UserId((long) id));
        user.setName(name);
        return user;
    }

    public void switchUserAdmin(){
        when(userServiceMock.getUser()).thenReturn(user(1,"admin"));
        when(userServiceMock.isLoggedIn()).thenReturn(true);
    }

    public void switchToPeterDefault(){
        when(userServiceMock.getUser()).thenReturn(user(2,"peter"));
        when(userServiceMock.isLoggedIn()).thenReturn(true);
    }

    public void switchToMax(){
        when(userServiceMock.getUser()).thenReturn(user(3,"max"));
        when(userServiceMock.isLoggedIn()).thenReturn(true);
    }

    public Given logged_in_user() {
        switchToPeterDefault();
        return this;
    }



    public Given empty_database() {
        return this;
    }

    public void spoonacular_has_bolognese() {
        when(spoonacularClientMock.searchRecipesAsString("Bolognese")).thenReturn(Parser.toJsonString(new SpoonacularRecipeSearchResultList(
                List.of(
                        new SpoonacularRecipeSearchResult(1, "Bolognese","https://spoonacular.com/recipeImages/634900-556x370.jpg","png",45, """
                                Do this then That
                                """)

                ))
        ));
        when(spoonacularClientMock.getRecipeByIdAsString("1", true)).thenReturn(
                """
                        {"title":"Bolognese","author": "Foodista","imageUrl": "https://spoonacular.com/recipeImages/634900-556x370.jpg","ingredients": []}
                        """
        );
        when(spoonacularClientMock.getRecipeByIdAsString("1", false)).thenThrow(
                UnsupportedOperationException.class
        );
    }


    public void bolognese_in_database() {
        var e = ExampleData.pastaEntity(1L);
        when(recipeDBRepository.findByTitle("Bolognese")).thenReturn(List.of(e));
        when(recipeDBRepository.findById(1L)).thenReturn(e);
    }
}
