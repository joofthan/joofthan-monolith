package com.joofthan.apps.mealplanner;

import com.joofthan.BaseTest;
import com.joofthan.blackbox.lib.TestLibrary;
import io.quarkus.test.junit.QuarkusTest;
import jakarta.ws.rs.*;
import lombok.Builder;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * Authentication is required
 * Get Token:
 * http://localhost:8080/q/swagger-ui/#/Login%20Endpoint/post_login_api_token
 * Set Token:
 * https://www.javacodemonk.com/retrofit-oauth2-bearer-token-authentication-okhttp-android-3b702350
 *
 */
@Disabled
@QuarkusTest
public class ShoppingItemsEndpointIT extends BaseTest {

    ShoppingService client;

    @BeforeEach
    void init(){
        client = TestLibrary.create(ShoppingService.class);
    }

    @Test
    void createUpdateDelete(){
        client.createItem(ShoppingItem.builder()
                        .id("123")
                        .amount(1)
                        .title("Apple")
                        .unparsedUserInput("1 Apple")
                .build());
        assertEquals(1,client.fetchItems().size());
        assertEquals("Apple", client.fetchItems().get(0).title());
        assertEquals("FRUIT", client.fetchItems().get(0).category());
        client.updateItem("123",ShoppingItem.builder()
                    .id("123")
                    .amount(1)
                    .title("abcdefg")
                    .unparsedUserInput("1 abcdefg")
                .build());
        assertEquals("abcdefg", client.fetchItems().get(0).title());
        assertEquals("UNKNOWN", client.fetchItems().get(0).category());
        client.deleteItem("123");
        assertEquals(0,client.fetchItems().size());
    }

    @Test
    void permissions(){
        switchToPeter();
        client.createItem(ShoppingItem.builder().id("500").title("titleFromPeter").build());
        assertEquals(1,client.fetchItems().size(), "should be created");
        switchToMax();
        assertEquals(0,client.fetchItems().size(), "should not be visible to other user");
        assertThrows(Exception.class, ()-> client.createItem(ShoppingItem.builder().id("500").build()), "should not be overrideable from other user");
        assertThrows(Exception.class, ()-> client.updateItem("500",ShoppingItem.builder().title("jello").build()), "should not be overrideable from other user");
        assertThrows(Exception.class, ()-> client.deleteItem("500"), "should not be deletable from other user");
        assertEquals(0,client.fetchItems().size(), "should not be accessible after executing these methods");
        switchToPeter();
        assertEquals(1,client.fetchItems().size(), "should be visible to user who created");
        assertEquals("titleFromPeter",client.fetchItems().get(0).title, "should not be changed after malicious requests");
        client.updateItem("500",ShoppingItem.builder().title("myo").build());
        assertEquals("myo",client.fetchItems().get(0).title, "should be updatable from user who created");
        client.deleteItem("500");
        assertEquals(0,client.fetchItems().size(), "should be deletable from user who created");
    }


    @Path("/mealplanner/api/shopping")
    public interface ShoppingService {
        @GET
        List<ShoppingItem> fetchItems();
        @POST
        void createItem(ShoppingItem item);
        @PUT
        @Path("{id}")
        void updateItem(@PathParam("id") String id, ShoppingItem item);
        @DELETE
        @Path("{id}")
        void deleteItem(@PathParam("id") String id);
    }

    @Builder
    public record ShoppingItem(
            String id,
            String title,
            String unit,
            float amount,
            boolean isMarked,
            String category,
            String unparsedUserInput
    ) {}

}