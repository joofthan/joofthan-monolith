package com.joofthan.apps.mealplanner;


import com.joofthan.BaseTest;
import com.joofthan.apps.mealplanner.backend.MealPlannerEndpoint;
import com.joofthan.apps.mealplanner.backend.food.MealRequest;
import com.joofthan.apps.mealplanner.backend.food.MealTimeLabeling;
import io.quarkus.test.junit.QuarkusTest;
import jakarta.inject.Inject;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;

@QuarkusTest
public class MealplannerMealApiV1Test extends BaseTest {


    @Inject
    MealPlannerEndpoint mealPlannerEndpoint;

    private OffsetDateTime someDate = OffsetDateTime.of(2022, 1, 1, 1, 1, 1, 1, ZoneOffset.UTC);

    @BeforeEach
    void cleanup(){
        //remove example data
        mealPlannerEndpoint.deleteMealById("1");
        mealPlannerEndpoint.deleteMealById("2");
    }

    @Test
    void addChangeDeleteMeal(){
        String myId = UUID.randomUUID().toString();

        //create
        assertEquals(0, mealPlannerEndpoint.getMeals().size());
        mealPlannerEndpoint.putMealById(myId, breakfastRequest("Hello World"));
        assertEquals(1, mealPlannerEndpoint.getMeals().size());

        //get
        assertEquals(myId, mealPlannerEndpoint.getMeals().get(0).mealId());
        assertEquals("Hello World", mealPlannerEndpoint.getMeals().get(0).title());

        //change
        mealPlannerEndpoint.putMealById(myId,breakfastRequest("OtherTile"));
        assertEquals("OtherTile", mealPlannerEndpoint.getMeals().get(0).title());

        //delete
        mealPlannerEndpoint.deleteMealById(myId);
        assertEquals(0, mealPlannerEndpoint.getMeals().size());
    }

    private MealRequest breakfastRequest(String title) {
        return new MealRequest(
                title,
                MealTimeLabeling.BREAKFAST,
                someDate,
                someDate,
                null,
                false
        );
    }

}
