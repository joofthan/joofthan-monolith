package com.joofthan.apps.mealplanner;


import com.joofthan.apps.mealplanner.cookingapi.sources.foodjson.FoodJsonApi;
import com.joofthan.apps.mealplanner.dsl.MealplannerDslBase;
import io.quarkus.test.junit.QuarkusTest;
import jakarta.inject.Inject;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

@QuarkusTest
public class FoodJsonApiITTest extends MealplannerDslBase {

    @Inject
    FoodJsonApi foodJsonApi;

    @Disabled()
    @Test
    void test() throws IOException {
       foodJsonApi.doImport();
               fail();
    }

    @Disabled("wip: not working in ci")
    @Test
    void search() throws IOException {
        assertEquals(1,foodJsonApi.search("Apricots, raw").size());
        assertEquals(0, foodJsonApi.search("Apricotesfdsf").size());
        assertEquals(16,foodJsonApi.search("Apricots").size());
    }
}
