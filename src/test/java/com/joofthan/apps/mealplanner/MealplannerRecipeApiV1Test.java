package com.joofthan.apps.mealplanner;


import com.joofthan.apps.mealplanner.dsl.MealplannerDslBase;
import org.junit.jupiter.api.Test;

public class MealplannerRecipeApiV1Test extends MealplannerDslBase {

    @Test
    void should_return_recipes_when_spoonacular_available(){
        given()
                .logged_in_user()
                .empty_database()
                .spoonacular_has_bolognese();
        when()
                .user_searches_recipe("Bolognese");
        then()
                .response_is_spoonacular_bolognese();
    }

    @Test
    void should_return_no_recipes_when_spoonacular_not_available(){
        given()
                .logged_in_user()
                .empty_database();
        when()
                .user_searches_recipe("Bolognese");
        then()
                .responseContains("[]");
    }

    @Test
    void should_return_recipe_details_when_spoonacular_has_recipe(){
        given()
                .logged_in_user()
                .empty_database()
                .spoonacular_has_bolognese();
        when()
                .user_gets_bolognese_details_by_id("spoonacular-1");
        then()
                .responseIs("""
                        {"ingredients":[],"name":"Bolognese","recipeId":"spoonacular-1","source":"SPOONACULAR_API"}
                        """);
    }

    @Test
    void should_return_recipe_details_when_database_has_recipe(){
        given()
                .logged_in_user()
                .bolognese_in_database();
        when()
                .user_gets_bolognese_details_by_id("database-1");
        then()
                .responseIs("""
                        {"imageUrl":"http://joofthan.com/images/locked.png","ingredients":[],"name":"Test Bolognese","recipeId":"database-1","servings":1,"source":"FLAVOUR_DB"}
                        """);
    }
}
