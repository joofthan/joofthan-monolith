package com.joofthan.services.config;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Config {

    String UNCONFIGURED_VALUE = "com.joofthan.simpleconfig.unconfigured.value";

    String value();
    String fallback() default UNCONFIGURED_VALUE;
    boolean required() default true;
}
