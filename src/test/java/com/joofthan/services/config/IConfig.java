package com.joofthan.services.config;

public interface IConfig {
    String get(String key);
    String get(String key, String fallback);
    String getOrNull(String key);

    void inject(Object obj);
}
