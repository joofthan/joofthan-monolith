package com.joofthan.services.config;

import java.lang.reflect.Field;

public class SimpleConfig implements IConfig {
    @Override
    public String get(String key) {
        String value = getOrNull(key);
        if(value == null){
            throw new ValueNotFoundException(key);
        }
        return value;
    }

    @Override
    public String get(String key, String fallback) {
        String value = getOrNull(key);
        if(value == null){
            return fallback;
        }
        return value;
    }

    @Override
    public String getOrNull(String key) {

        String envValue = System.getenv(key);
        if(envValue != null){
            return envValue;
        }

        String propValue = System.getProperty(key);
        if(propValue != null){
            return propValue;
        }

        return null;
    }

    @Override
    public void inject(Object obj) {
        for (Field field : obj.getClass().getDeclaredFields()) {
            if (field.isAnnotationPresent(Config.class)) {
                Config ann = field.getAnnotation(Config.class);
                try {
                    String value = null;
                    if(!ann.fallback().equals(Config.UNCONFIGURED_VALUE)){
                        field.set(obj,get(ann.value(), ann.fallback()));
                    }else if(ann.required()){
                        field.set(obj, get(ann.value()));
                    }else {
                        field.set(obj, getOrNull(ann.value()));
                    }
                } catch (IllegalAccessException e) {
                    throw new RuntimeException(e);
                }
            }
        }
    }
}
