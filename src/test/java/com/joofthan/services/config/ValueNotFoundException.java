package com.joofthan.services.config;

public class ValueNotFoundException extends RuntimeException {
    public ValueNotFoundException(String key) {
        super("\""+key+"\" not found in System Properties or Environment Variables");
    }
}
