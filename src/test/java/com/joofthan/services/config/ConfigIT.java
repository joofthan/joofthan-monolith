package com.joofthan.services.config;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ConfigIT {
    IConfig config = new SimpleConfig();

    @Test
    void getException(){
        assertThrows(ValueNotFoundException.class,()-> config.get("hello"));
    }

    @Test
    void getDefault(){
        String result = config.get("hello", "someDefault");
        assertEquals("someDefault", result);

    }
    @Test
    void getNull(){
        String result = config.getOrNull("hit");
        assertNull(result);

    }

    @BeforeEach
    void initInject(){
        System.setProperty("doesExist", "existingValue");
        config.inject(this);
    }

    @Config("doesExist")
    String exis;
    @Test
    void getExisting(){
        assertEquals("existingValue", exis);
    }

    @Config(value = "va", fallback = "someDefault")
    String defa;
    @Test
    void getDefault2(){
        assertEquals("someDefault", defa);
    }

    @Config(value = "va", required = false)
    String isNul;
    @Test
    void getNull2(){
        assertNull(isNul);
    }

    @Test
    void getException2(){
        HasException obj = new HasException();
        assertThrows(ValueNotFoundException.class,()-> config.inject(obj));
    }

    class HasException {
        @Config("va")
        String ex;
    }
}
